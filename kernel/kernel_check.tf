$
$  TOPPERS ATK2
$      Toyohashi Open Platform for Embedded Real-Time Systems
$      Automotive Kernel Version 2
$
$  Copyright (C) 2008-2013 by Center for Embedded Computing Systems
$              Graduate School of Information Science, Nagoya Univ., JAPAN
$  Copyright (C) 2011-2013 by FUJISOFT INCORPORATED, JAPAN
$  Copyright (C) 2011-2013 by FUJITSU VLSI LIMITED, JAPAN
$  Copyright (C) 2011-2013 by NEC Communication Systems, Ltd., JAPAN
$  Copyright (C) 2011-2013 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
$  Copyright (C) 2011-2013 by Renesas Electronics Corporation, JAPAN
$  Copyright (C) 2011-2013 by Sunny Giken Inc., JAPAN
$  Copyright (C) 2011-2013 by TOSHIBA CORPORATION, JAPAN
$  Copyright (C) 2011-2013 by Witz Corporation, JAPAN
$
$  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
$  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
$  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
$  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
$      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
$      スコード中に含まれていること．
$  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
$      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
$      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
$      の無保証規定を掲載すること．
$  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
$      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
$      と．
$    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
$        作権表示，この利用条件および下記の無保証規定を掲載すること．
$    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
$        報告すること．
$  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
$      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
$      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
$      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
$      免責すること．
$
$  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
$  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
$  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
$  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
$  の責任を負わない．
$
$  $Id: kernel_check.tf 6837 2013-01-23 10:46:48Z fsi-dankei $
$

$
$  関数の先頭番地のチェック
$
$IF CHECK_FUNC_ALIGN || CHECK_FUNC_NONNULL$
$	// タスクの先頭番地のアラインチェック
	$tinib = SYMBOL("tinib_table")$
	$FOREACH tskid TSK.ID_LIST$
		$task = PEEK(tinib + offsetof_TINIB_task, sizeof_FunctionRefType)$
		$IF CHECK_FUNC_ALIGN && (task & (CHECK_FUNC_ALIGN - 1)) != 0$
			$ERROR TSK.TEXT_LINE[tskid]$
				$FORMAT(_("%1% of %2% in `%3%\' is not aligned"),
				"task", "TINIB", tskid)$$END$
		$END$
$		// タスクの先頭番地のNULLチェック
		$IF CHECK_FUNC_NONNULL && task == 0$
			$ERROR TSK.TEXT_LINE[tskid]$
				$FORMAT(_("%1% of %2% in `%3%\' is null"),
				"task", "TINIB", tskid)$$END$
		$END$
		$tinib = tinib + sizeof_TINIB$
	$END$

$	//ハードウェアカウンタの処理関数
	$HWCNTFUNC_VALID = { VALUE("init",0),VALUE("start",1),VALUE("stop",2),VALUE("set",3),VALUE("get",4),
	VALUE("cancel",5),VALUE("trigger",6),VALUE("intclear",7),VALUE("intcancel",8),VALUE("increment",9) }$

$	// ハードウェアカウンタの処理関数のアラインチェック
	$hwcntinib = SYMBOL("hwcntinib_table")$
	$FOREACH hwcntid HWCNT.ID_LIST$
		$FOREACH memberid HWCNTFUNC_VALID$
			$offsetofmember = offsetof_HWCNTINIB_init + (sizeof_FunctionRefType * memberid)$
			$hwfunc = PEEK(hwcntinib + offsetofmember, sizeof_FunctionRefType)$
			$IF CHECK_FUNC_ALIGN && (hwfunc & (CHECK_FUNC_ALIGN - 1)) != 0$
				$ERROR HWCNT.TEXT_LINE[hwcntid]$
					$FORMAT(_("%1% of %2% in `%3%\' is not aligned"),
					memberid, "HWCNTINIB", hwcntid)$$END$
			$END$
$			// ハードウェアカウンタの処理関数のNULLチェック
			$IF CHECK_FUNC_NONNULL && hwfunc == 0$
				$ERROR HWCNT.TEXT_LINE[hwcntid]$
					$FORMAT(_("%1% of %2% in `%3%\' is null"),
					memberid, "HWCNTINIB", hwcntid)$$END$
			$END$
		$END$
		$hwcntinib = hwcntinib + sizeof_HWCNTINIB$
	$END$
$END$

$
$  スタック領域の先頭番地のチェック
$
$IF CHECK_STACK_ALIGN || CHECK_STACK_NONNULL$
	$tinib = SYMBOL("tinib_table")$
	$FOREACH tskid TSK.ID_LIST$
$		// タスクのシステムスタック領域の先頭番地のチェック
		$IF USE_TSKINICTXB$
			$sstk = GET_SSTK_TSKINICTXB(tinib)$
		$ELSE$
			$sstk = PEEK(tinib + offsetof_TINIB_sstk, sizeof_void_ptr)$
		$END$
		$IF CHECK_STACK_ALIGN && (sstk & (CHECK_STACK_ALIGN - 1)) != 0$
			$IF OSAP.TRUSTED[TSK.OSAPID[tskid]]$
				$ERROR TSK.TEXT_LINE[tskid]$
					$FORMAT(_("%1% `%2%\' of `%3%\' is not aligned"),
					"stk", TSK.STK[tskid], tskid)$$END$
			$ELSE$
				$ERROR TSK.TEXT_LINE[tskid]$
					$FORMAT(_("%1% `%2%\' of `%3%\' is not aligned"),
					"sstk", TSK.SSTK[tskid], tskid)$$END$
			$END$
		$END$
		$IF CHECK_STACK_NONNULL && sstk == 0$
			$IF OSAP.TRUSTED[TSK.OSAPID[tskid]]$
				$ERROR TSK.TEXT_LINE[tskid]$
					$FORMAT(_("%1% `%2%\' of `%3%\' is null"),
					"stk", TSK.STK[tskid], tskid)$$END$
			$ELSE$
				$ERROR TSK.TEXT_LINE[tskid]$
					$FORMAT(_("%1% `%2%\' of `%3%\' is null"),
					"sstk", TSK.SSTK[tskid], tskid)$$END$
			$END$
		$END$

$		// タスクのユーザスタック領域の先頭番地のチェック
		$IF !OSAP.TRUSTED[TSK.OSAPID[tskid]]$
			$IF USE_TSKINICTXB$
				$ustk = GET_USTK_TSKINICTXB(tinib)$
			$ELSE$
				$ustk = PEEK(tinib + offsetof_TINIB_ustk, sizeof_void_ptr)$
			$END$
			$IF CHECK_USTACK_ALIGN && (ustk & (CHECK_USTACK_ALIGN - 1)) != 0$
				$ERROR TSK.TEXT_LINE[tskid]$
					$FORMAT(_("%1% `%2%\' of `%3%\' is not aligned"),
					"stk", TSK.STK[tskid], tskid)$$END$
			$END$
			$IF CHECK_USTACK_NONNULL && ustk == 0$
				$ERROR TSK.TEXT_LINE[tskid]$
					$FORMAT(_("%1% `%2%\' of `%3%\' is null"),
					"stk", TSK.STK[tskid], tskid)$$END$
			$END$
		$END$
		$tinib = tinib + sizeof_TINIB$
	$END$

$	// 非タスクコンテキスト用のスタック領域の先頭番地のチェック
	$ostk_addr = PEEK(SYMBOL("_ostk"), sizeof_void_ptr)$
	$IF CHECK_STACK_ALIGN && (ostk_addr & (CHECK_STACK_ALIGN - 1)) != 0$
		$ERROR OSTK.TEXT_LINE[1]$
			$FORMAT(_("%1% `%2%\' is not aligned"),
			"stk", OSTK.STK[1])$$END$
	$END$
	$IF CHECK_STACK_NONNULL && ostk_addr == 0$
		$ERROR OSTK.TEXT_LINE[1]$
			$FORMAT(_("%1% `%2%\' is null"),
			"stk", OSTK.STK[1])$$END$
	$END$

$	// 非信頼OSAPフックルーチン用のスタック領域の先頭番地のチェック
	$IF USE_NONTRUST_HOOK$
		$IF LENGTH(NTHSTK.STK[1]) && !EQ(NTHSTK.STK[1], "NULL")$
			$nthstk_addr = PEEK(SYMBOL("_nthkstk"), sizeof_void_ptr)$
			$IF CHECK_USTACK_ALIGN && (nthstk_addr & (CHECK_USTACK_ALIGN - 1)) != 0$
				$ERROR NTHSTK.TEXT_LINE[1]$
					$FORMAT(_("%1% `%2%\' is not aligned %3%"),
					"NonTrustedHookStack", NTHSTK.STK[1], +CHECK_USTACK_ALIGN)$$END$
			$END$

			$IF CHECK_STACK_NONNULL && nthstk_addr == 0$
				$ERROR NTHSTK.TEXT_LINE[1]$
					$FORMAT(_("%1% `%2%\' is null"),
					"NonTrustedHookStack", NTHSTK.STK[1])$$END$
			$END$
		$END$
	$END$
$END$

$tfinib_table = SYMBOL("tfinib_table")$
$FOREACH tfnid TFN.ID_LIST$
	$TrustedFunction = PEEK(tfinib_table, sizeof_TrustedFunctionRefType)$
$	// 信頼関数の先頭番地のアラインチェック
	$IF CHECK_FUNC_ALIGN && (task & (CHECK_FUNC_ALIGN - 1)) != 0$
		$ERROR TFN.TEXT_LINE[tfnid]$
			$FORMAT(_("%1% of %2% in `%3%\' is not aligned"),
			"trs_func", "TFINIB", TFN.TFN[tfnid])$$END$
	$END$
$	// 信頼関数のNULLチェック
	$IF TrustedFunction == 0$
		$ERROR TFN.TEXT_LINE[tfnid]$
			$FORMAT(_("%1% of %2% in `%2%\' is null"),
			"trs_func", "TFINIB", TFN.TFN[tfnid])$$END$
	$END$
	$tfinib_table = tfinib_table + sizeof_TFINIB$
$END$
