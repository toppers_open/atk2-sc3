$
$  TOPPERS ATK2
$      Toyohashi Open Platform for Embedded Real-Time Systems
$      Automotive Kernel Version 2
$
$  Copyright (C) 2011-2013 by Center for Embedded Computing Systems
$              Graduate School of Information Science, Nagoya Univ., JAPAN
$  Copyright (C) 2011-2013 by FUJISOFT INCORPORATED, JAPAN
$  Copyright (C) 2011-2013 by FUJITSU VLSI LIMITED, JAPAN
$  Copyright (C) 2011-2013 by NEC Communication Systems, Ltd., JAPAN
$  Copyright (C) 2011-2013 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
$  Copyright (C) 2011-2013 by Renesas Electronics Corporation, JAPAN
$  Copyright (C) 2011-2013 by Sunny Giken Inc., JAPAN
$  Copyright (C) 2011-2013 by TOSHIBA CORPORATION, JAPAN
$  Copyright (C) 2011-2013 by Witz Corporation, JAPAN
$
$  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
$  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
$  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
$  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
$      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
$      スコード中に含まれていること．
$  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
$      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
$      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
$      の無保証規定を掲載すること．
$  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
$      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
$      と．
$    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
$        作権表示，この利用条件および下記の無保証規定を掲載すること．
$    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
$        報告すること．
$  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
$      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
$      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
$      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
$      免責すること．
$
$  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
$  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
$  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
$  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
$  の責任を負わない．
$
$  $Id: kernel_mem.tf 7284 2013-03-29 06:18:22Z fujitsu-wada $
$

$CFG_PASS4 = 0$
$
$  パス2からの情報の読込み
$
$INCLUDE "cfg2_out.tf"$
$INCLUDE "kernel/kernel_common.tf"$


$FILE "kernel_mem3.c"$
/* kernel_mem3.c */$NL$
#include "kernel/kernel_int.h"$NL$
#include "Os_Lcfg.h"$NL$
#ifndef TOPPERS_EMPTY_LABEL$NL$
#define TOPPERS_EMPTY_LABEL(x, y) x y[0]$NL$
#endif$NL$
$NL$

/*$NL$
$SPC$*  Include Directives (#include)$NL$
$SPC$*/$NL$
$NL$
$INCLUDES$
$NL$

$ MO.BASEADDR, MO.LIMITADDRの設定
$SET_MO_ADDR()$

$ MO_MEMTOP_ORDER_MEMINIBの作成
$SET_MEMTOP_ORDER()$

$ MO_MEMTOP_ORDER_MEMINIBの統合
$MERGE_MEMINIB()$

$ セクションの開始，終端のラベルをextern宣言する
$EXTERN_SECTION_LABEL()$

$IF !OMIT_STANDARD_MEMINIB$
$ memtop_table, meminib_tableの出力
$GENERATE_MEMINIB_TABLE("")$
$END$

$ データセクション初期化テーブルを出力する(kernel_common.tf)
$GENERATE_DATASECINIB_TABLE()$

$ BSSセクション初期化テーブルを出力する(kernel_common.tf)
$GENERATE_BSSSECINIB_TABLE()$

$ ターゲット依存部で必要なMPUINFOBを出力する
$GENERATE_TARGET_MPUINFOB()$

$ タスクスタック領域をextern宣言する(kernel_common.tf)
$GENERATE_EXPORT_TSK_STK()$

$ タスクをextern宣言する(kernel_common.tf)
$EXTERN_TSK()$

$ タスク初期化ブロックを出力する(kernel_common.tf)
$GENERATE_TINIB_TABLE()$

$ OSアプリケーション初期化ブロックを出力する(kernel_common.tf)
$GENERATE_OSAPINIB_TABLE()$

$ メモリオブジェクトのアドレスを出力
$OUTPUT_MO_ADDR()$
