$
$  TOPPERS ATK2
$      Toyohashi Open Platform for Embedded Real-Time Systems
$      Automotive Kernel Version 2
$
$  Copyright (C) 2011-2013 by Center for Embedded Computing Systems
$              Graduate School of Information Science, Nagoya Univ., JAPAN
$  Copyright (C) 2011-2013 by FUJISOFT INCORPORATED, JAPAN
$  Copyright (C) 2011-2013 by FUJITSU VLSI LIMITED, JAPAN
$  Copyright (C) 2011-2013 by NEC Communication Systems, Ltd., JAPAN
$  Copyright (C) 2011-2013 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
$  Copyright (C) 2011-2013 by Renesas Electronics Corporation, JAPAN
$  Copyright (C) 2011-2013 by Sunny Giken Inc., JAPAN
$  Copyright (C) 2011-2013 by TOSHIBA CORPORATION, JAPAN
$  Copyright (C) 2011-2013 by Witz Corporation, JAPAN
$
$  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
$  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
$  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
$  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
$      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
$      スコード中に含まれていること．
$  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
$      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
$      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
$      の無保証規定を掲載すること．
$  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
$      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
$      と．
$    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
$        作権表示，この利用条件および下記の無保証規定を掲載すること．
$    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
$        報告すること．
$  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
$      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
$      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
$      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
$      免責すること．
$
$  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
$  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
$  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
$  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
$  の責任を負わない．
$
$  $Id: kernel_xml.tf 6837 2013-01-23 10:46:48Z fsi-dankei $
$

$ =====================================================================
$ kernel.tfの処理の前に処理されるファイル
$ XMLのデータ構造で足りないデータの生成処理
$ 共通化できないエラー処理
$ =====================================================================

$TRACE("EXEC kernel_xml.tf")$

$ kernel.tfを実行できないようなエラーが発生したか
$FATAL = 0$


$ =====================================================================
$ OSAPのエラーチェックと関連づけ
$ =====================================================================

$FOREACH osapid OSAP.ID_LIST$
	$FOREACH tskid OSAP.TSK_LIST[osapid]$
		$IF LENGTH(TSK.OSAPID[tskid])$
			$ERROR OSAP.TEXT_LINE[osapid]$$FORMAT(_("OsTask(%1%) belongs to another OsApplication(%2%)"), tskid, TSK.OSAPID[tskid])$$END$
		$ELSE$
			$TSK.OSAPID[tskid] = osapid$
		$END$
	$END$

	$FOREACH cntid OSAP.CNT_LIST[osapid]$
		$IF LENGTH(CNT.OSAPID[cntid])$
			$ERROR OSAP.TEXT_LINE[osapid]$$FORMAT(_("OsCounter(%1%) belongs to another OsApplication(%2%)"), cntid, CNT.OSAPID[cntid])$$END$
		$ELSE$
			$CNT.OSAPID[cntid] = osapid$
		$END$
	$END$

	$FOREACH almid OSAP.ALM_LIST[osapid]$
		$IF LENGTH(ALM.OSAPID[almid])$
			$ERROR OSAP.TEXT_LINE[osapid]$$FORMAT(_("OsAlarm(%1%) belongs to another OsApplication(%2%)"), almid, ALM.OSAPID[almid])$$END$
		$ELSE$
			$ALM.OSAPID[almid] = osapid$
		$END$
	$END$

	$FOREACH isrid OSAP.ISR_LIST[osapid]$
		$IF LENGTH(ISR.OSAPID[isrid])$
			$ERROR OSAP.TEXT_LINE[osapid]$$FORMAT(_("OsIsr(%1%) belongs to another OsApplication(%2%)"), isrid, ISR.OSAPID[isrid])$$END$
		$ELSE$
			$ISR.OSAPID[isrid] = osapid$
		$END$
	$END$

	$FOREACH schtblid OSAP.SCHTBL_LIST[osapid]$
		$IF LENGTH(SCHTBL.OSAPID[schtblid])$
			$ERROR OSAP.TEXT_LINE[osapid]$$FORMAT(_("OsScheduleTable(%1%) belongs to another OsApplication(%2%)"), schtblid, SCHTBL.OSAPID[schtblid])$$END$
		$ELSE$
			$SCHTBL.OSAPID[schtblid] = osapid$
		$END$
	$END$

	$FOREACH secid OSAP.SEC_LIST[osapid]$
		$IF LENGTH(SEC.OSAPID[secid])$
			$ERROR OSAP.TEXT_LINE[osapid]$$FORMAT(_("OsMemorySection(%1%) belongs to another OsApplication(%2%)"),secid, SEC.OSAPID[secid])$$END$
		$ELSE$
			$SEC.OSAPID[secid] = osapid$
		$END$
	$END$

	$FOREACH modid OSAP.MOD_LIST[osapid]$
		$IF LENGTH(MOD.OSAPID[modid])$
			$ERROR OSAP.TEXT_LINE[osapid]$$FORMAT(_("OsMemoryModule(%1%) belongs to another OsApplication(%2%)"), modid, MOD.OSAPID[modid])$$END$
		$ELSE$
			$MOD.OSAPID[modid] = osapid$
		$END$
	$END$

	$FOREACH memid OSAP.MEM_LIST[osapid]$
		$IF LENGTH(MEM.OSAPID[memid])$
			$ERROR OSAP.TEXT_LINE[osapid]$$FORMAT(_("OsMemoryArea(%1%) belongs to another OsApplication(%2%)"), memid, MEM.OSAPID[memid])$$END$
		$ELSE$
			$MEM.OSAPID[memid] = osapid$
		$END$
	$END$
$END$

$ OSAP.HOOK.*をOSAP.*にコピー
$FOREACH hookid OSAP.HOOK.ID_LIST$
	$osapid = OSAP.HOOK.PARENT[hookid]$
	$OSAP.STARTUPHOOK[osapid] = OSAP.HOOK.STARTUPHOOK[hookid]$
	$OSAP.SHUTDOWNHOOK[osapid] = OSAP.HOOK.SHUTDOWNHOOK[hookid]$
	$OSAP.ERRORHOOK[osapid] = OSAP.HOOK.ERRORHOOK[hookid]$
$END$

$ OSAP.TFN.*をTFN.*にコピー
$FOREACH tfnid OSAP.TFN.ID_LIST$
	$osapid = OSAP.TFN.PARENT[tfnid]$
	$TFN.ID_LIST = APPEND(TFN.ID_LIST, tfnid)$
	$TFN.FUNC[tfnid] = OSAP.TFN.FUNC[tfnid]$
	$TFN.STKSZ[tfnid] = OSAP.TFN.STKSZ[tfnid]$
	$TFN.OSAPID[tfnid] = osapid$
$END$


$ =====================================================================
$ OSTK.STKがない場合にNULLを入れる
$ =====================================================================

$FOREACH ostkid OSTK.ID_LIST$
	$IF !LENGTH(OSTK.STK[ostkid])$
		$OSTK.STK[ostkid] = "NULL"$
	$END$
$END$


$ =====================================================================
$ NTHSTK.STKがない場合にNULLを入れる
$ =====================================================================

$FOREACH nthstkid NTHSTK.ID_LIST$
	$IF !LENGTH(NTHSTK.STK[nthstkid])$
		$NTHSTK.STK[nthstkid] = "NULL"$
	$END$
$END$


$ =====================================================================
$ REG.REGATRとREG.REGNAMEの作成
$ REG.REGIONとREG.ID_LISTを静的APIの形式に合わせる
$ =====================================================================

$new_id_list = {}$
$FOREACH regid REG.ID_LIST$
	$IF EQ(REG.TYPE[regid], "STANDARD_ROM")$
		$REG.REGATR[regid] = VALUE("TA_STDROM", TA_STDROM)$
		$IF REG.WRITE[regid]$
			$FATAL = 1$
			$ERROR REG.TEXT_LINE[regid]$$FORMAT(_("STANDARD_ROM must not be writable"))$$END$
		$END$
	$ELIF EQ(REG.TYPE[regid], "STANDARD_RAM")$
		$REG.REGATR[regid] = VALUE("TA_STDRAM", TA_STDRAM)$
		$IF !REG.WRITE[regid]$
			$FATAL = 1$
			$ERROR REG.TEXT_LINE[regid]$$FORMAT(_("STANDARD_RAM must be writable"))$$END$
		$END$
	$ELIF EQ(REG.TYPE[regid], "NO_STANDARD")$
		$REG.REGATR[regid] = VALUE("TA_NULL", TA_NULL)$
	$ELSE$
		$REG.REGATR[regid] = 0$
		$ERROR REG.TEXT_LINE[regid]$$FORMAT(_("OsMemoryRegionAttribute must be STANDARD_ROM or STANDARD_RAM or NO_STANDARD"))$$END$
	$END$

	$IF !REG.WRITE[regid]$
		$REG.REGATR[regid] = REG.REGATR[regid] | TA_NOWRITE$
	$END$

	$REG.REGION[regid] = ESCSTR(REG.REGION[regid])$
	$new_id_list = APPEND(new_id_list, +regid)$
$END$
$REG.ID_LIST = new_id_list$
$REG.ORDER_LIST = new_id_list$


$ =====================================================================
$ SECのエラーチェックとSEC.MEMATRの作成
$ =====================================================================

$FOREACH secid SEC.ID_LIST$
	$SEC.MEMATR[secid] = 0$

	$IF LENGTH(SEC.INIT[secid])$
		$IF EQ(SEC.INIT[secid], "DATA")$
			$SEC.MEMATR[secid] = TA_MEMINI$
		$ELIF EQ(SEC.INIT[secid], "BSS")$
			$SEC.MEMATR[secid] = TA_NULL$
		$ELIF EQ(SEC.INIT[secid], "NO_INTIALIZE")$
			$SEC.MEMATR[secid] = TA_MEMPRSV$
		$ELSE$
			$ERROR SEC.TEXT_LINE[secid]$$FORMAT(_("OsMemorySectionInitialize must be DATA or BSS or NO_INTIALIZE"))$$END$
		$END$
		$IF !REG.WRITE[SEC.MEMREG[secid]]$
			$ERROR SEC.TEXT_LINE[secid]$$FORMAT(_("OsMemorySectionInitialize must not be defined on nonwritable memory region"))$$END$
		$END$
	$ELSE$
		$IF REG.WRITE[SEC.MEMREG[secid]]$
			$ERROR SEC.TEXT_LINE[secid]$$FORMAT(_("OsMemorySectionInitialize must be defined on writable memory region"))$$END$
		$END$
	$END$

	$IF !SEC.WRITE[secid]$
		$SEC.MEMATR[secid] = SEC.MEMATR[secid] | TA_NOWRITE$
	$END$
	$IF !SEC.READ[secid]$
		$SEC.MEMATR[secid] = SEC.MEMATR[secid] | TA_NOREAD$
	$END$
	$IF SEC.EXEC[secid]$
		$SEC.MEMATR[secid] = SEC.MEMATR[secid] | TA_EXEC$
	$END$
	$IF SEC.SDATA[secid]$
		$SEC.MEMATR[secid] = SEC.MEMATR[secid] | TA_SDATA$
	$END$
	$IF !SEC.CACHE[secid]$
		$SEC.MEMATR[secid] = SEC.MEMATR[secid] | TA_UNCACHE$
	$END$
	$IF SEC.DEVICE[secid]$
		$SEC.MEMATR[secid] = SEC.MEMATR[secid] | TA_IODEV$
	$END$
$END$


$ =====================================================================
$ MEM.MEMATRの作成
$ =====================================================================

$FOREACH memid MEM.ID_LIST$
	$MEM.MEMATR[memid] = TA_MEMPRSV$
	$IF !MEM.WRITE[memid]$
		$MEM.MEMATR[memid] = MEM.MEMATR[memid] | TA_NOWRITE$
	$END$
	$IF !MEM.READ[memid]$
		$MEM.MEMATR[memid] = MEM.MEMATR[memid] | TA_NOREAD$
	$END$
	$IF MEM.EXEC[memid]$
		$MEM.MEMATR[memid] = MEM.MEMATR[memid] | TA_EXEC$
	$END$
	$IF !MEM.CACHE[memid]$
		$MEM.MEMATR[memid] = MEM.MEMATR[memid] | TA_UNCACHE$
	$END$
	$IF MEM.DEVICE[memid]$
		$MEM.MEMATR[memid] = MEM.MEMATR[memid] | TA_IODEV$
	$END$
$END$


$ =====================================================================
$ LNKをSECに統合する
$ =====================================================================

$IF LENGTH(SEC.ID_LIST)$
	$i = AT(SEC.ID_LIST, LENGTH(SEC.ID_LIST) - 1) + 1$
$ELSE$
	$i = 1$
$END$
$FOREACH lnkid LNK.ID_LIST$
	$SEC.SECTION[i] = LNK.SECTION[lnkid]$
	$SEC.MEMREG[i] = LNK.MEMREG[lnkid]$
	$SEC.ID_LIST = APPEND(SEC.ID_LIST, VALUE(lnkid, i))$
	$SEC.ORDER_LIST = APPEND(SEC.ORDER_LIST, VALUE(lnkid, i))$
	$i = i + 1$
$END$

$ MEMREGはREFになっているがkernel.tfでは文字列として扱う
$FOREACH secid SEC.ID_LIST$
	$SEC.MEMREG[secid] = REG.REGION[SEC.MEMREG[secid]]$
	$SEC.SECTION[secid] = ESCSTR(SEC.SECTION[secid])$
$END$


$ =====================================================================
$ MOD.MODULEの作成
$ =====================================================================

$FOREACH modid MOD.ID_LIST$
	$MOD.MODULE[modid] = ESCSTR(MOD.MODULE[modid])$
$END$


$ =====================================================================
$ TSKのエラーチェックと関連づけ
$ =====================================================================

$TRACE("ASSOCIATE TASK")$

$ TSK.AUTOSTARTをTSKにコピー
$FOREACH aid TSK.AUTOSTART.ID_LIST$
	$tskid                = TSK.AUTOSTART.PARENT[aid]$
	$TSK.APP_LIST[tskid]  = TSK.AUTOSTART.APP_LIST[aid]$
$END$


$FOREACH tskid TSK.ID_LIST$
	$IF LENGTH(TSK.OSAPID[tskid])$
		$IF OSAP.TRUSTED[TSK.OSAPID[tskid]]$
$			// 信頼タスクはOsTaskSystemStackStartAddressの使用不可
			$IF LENGTH(TSK.SSTK[tskid])$
				$ERROR TSK.TEXT_LINE[tskid]$$FORMAT(_("Trusted OsTask(%1%) can't use OsTaskSystemStackStartAddress"), tskid)$$END$
			$END$
		$ELSE$
$			// 非信頼タスクでシステムスタックの先頭番地が指定されてサイズが指定されない
			$IF LENGTH(TSK.SSTK[tskid]) && !LENGTH(TSK.SSTKSZ[tskid])$
				$ERROR TSK.TEXT_LINE[tskid]$$FORMAT(_("OsTaskSystemStackSize of Non-Trusted OsTask(%1%) is necessary when OsTaskSystemStackStartAddress is used"), tskid)$$END$
			$END$
		$END$
	$END$
$END$


$ =====================================================================
$ ISRのエラーチェックと関連づけ
$ =====================================================================

$TRACE("ASSOCIATE ISR")$

$FOREACH isrid ISR.ID_LIST$
	$IF !(EQ(ISR.SOURCE[isrid], "ENABLE") || EQ(ISR.SOURCE[isrid], "DISABLE"))$
		$ERROR ISR.TEXT_LINE[isrid]$$FORMAT(_("OsIsrInterruptSource must be ENABLE or DISABLE"))$$END$
	$END$

$	// C1ISRはENABLEにする必要がある
	$IF EQ(ISR.SOURCE[isrid], "DISABLE") && EQ(ISR.CATEGORY[isrid], "CATEGORY_1")$
		$ERROR ISR.TEXT_LINE[isrid]$$FORMAT(_("OsIsrInterruptSource of C1ISR OsIsr(%1%) must include ENABLE"), isrid)$$END$
	$END$

	$IF EQ(ISR.SOURCE[isrid], "ENABLE")$
		$IF LENGTH(ISR.INTATR[isrid])$
			$ISR.INTATR[isrid] = VALUE(CONCAT(ISR.INTATR[isrid], " | ENABLE"), ISR.INTATR[isrid] | ENABLE)$
		$ELSE$
			$ISR.INTATR[isrid] = ENABLE$
		$END$
	$END$

	$IF EQ(ISR.CATEGORY[isrid], "CATEGORY_1")$
		$IF LENGTH(ISR.STKSZ[isrid])$
			$ERROR ISR.TEXT_LINE[isrid]$$FORMAT(_("OsIsrStackSize of C1ISR OsIsr(%1%) must not define"), isrid)$$END$
		$END$
	$ELIF EQ(ISR.CATEGORY[isrid], "CATEGORY_2")$
		$IF LENGTH(ISR.STKSZ[isrid]) && (ISR.STKSZ[isrid] == 0)$
			$ERROR ISR.TEXT_LINE[isrid]$$FORMAT(_("OsIsrStackSize of C2ISR OsIsr(%1%) should not defined zero"), isrid)$$END$
		$END$
$		// C2ISRスタックサイズのデフォルト設定
		$IF !LENGTH(ISR.STKSZ[isrid])$
			$ISR.STKSZ[isrid] = DEFAULT_ISRSTKSZ$
		$END$
	$END$
$END$


$ =====================================================================
$ EVTに関する処理
$ =====================================================================

$FOREACH evtid EVT.ID_LIST$
$	// マスクが省略された場合はAUTOにする
	$IF !LENGTH(EVT.MASK[evtid])$
		$EVT.MASK[evtid] = AUTO$
	$END$
$END$


$ =====================================================================
$ CNTに関する処理
$ =====================================================================

$FOREACH cntid CNT.ID_LIST$
	$IF EQ(CNT.CNTATR[cntid], "HARDWARE")$
		$IF !LENGTH(CNT.ISRID[cntid])$
			$FATAL = 1$
			$ERROR CNT.TEXT_LINE[cntid]$$FORMAT(_("OsCounterIsrRef of HARDWARE OsCounter(%1%) is not defined"), cntid)$$END$
		$END$

$		// NSPERTICKをSECONDSPERTICKから計算する
		$IF LENGTH(CNT.SECONDSPERTICK[cntid])$
			$CNT.NSPERTICK[cntid] = CONCAT(CNT.SECONDSPERTICK[cntid], " * 1000000000")$
		$END$
	$END$

$END$


$ =====================================================================
$ ALMに関する処理
$ =====================================================================

$TRACE("ASSOCIATE ALARM")$

$ ALM.AUTOSTART.*からALM.*にコピー
$FOREACH aid ALM.AUTOSTART.ID_LIST$
	$almid                              = ALM.AUTOSTART.PARENT[aid]$
	$ALM.APP_LIST[almid]                = ALM.AUTOSTART.APP_LIST[aid]$
	$ALM.ALARMTIME[almid]               = ALM.AUTOSTART.ALARMTIME[aid]$
	$ALM.CYCLETIME[almid]               = ALM.AUTOSTART.CYCLETIME[aid]$
	$ALM.AUTOSTARTTYPE[almid]           = ALM.AUTOSTART.TYPE[aid]$
$END$

$FOREACH almid ALM.ID_LIST$
	$ALM.ALMATR_COUNT[almid] = 0$
$END$

$ ALM.ACTION.ACTIVATETASK.*からALM.*にコピー
$FOREACH almactid ALM.ACTION.ACTIVATETASK.ID_LIST$
	$almid                      = ALM.ACTION.PARENT[ALM.ACTION.ACTIVATETASK.PARENT[almactid]]$
	$ALM.ALMATR[almid]          = "ACTIVATETASK"$
	$ALM.TSKID[almid]           = ALM.ACTION.ACTIVATETASK.TSKID[almactid]$
	$ALM.ALMATR_COUNT[almid]    = ALM.ALMATR_COUNT[almid] + 1$
$END$

$ ALM.ACTION.SETEVENT.*からALM.*にコピー
$FOREACH almactid ALM.ACTION.SETEVENT.ID_LIST$
	$almid                      = ALM.ACTION.PARENT[ALM.ACTION.SETEVENT.PARENT[almactid]]$
	$ALM.ALMATR[almid]          = "SETEVENT"$
	$ALM.TSKID[almid]           = ALM.ACTION.SETEVENT.TSKID[almactid]$
	$ALM.EVTID[almid]           = ALM.ACTION.SETEVENT.EVTID[almactid]$
	$ALM.ALMATR_COUNT[almid]    = ALM.ALMATR_COUNT[almid] + 1$
$END$

$ ALM.ACTION.INCREMENTCOUNTER.*からALM.*にコピー
$FOREACH almactid ALM.ACTION.INCREMENTCOUNTER.ID_LIST$
	$almid                      = ALM.ACTION.PARENT[ALM.ACTION.INCREMENTCOUNTER.PARENT[almactid]]$
	$ALM.ALMATR[almid]          = "INCREMENTCOUNTER"$
	$ALM.INCID[almid]           = ALM.ACTION.INCREMENTCOUNTER.INCID[almactid]$
	$ALM.ALMATR_COUNT[almid]    = ALM.ALMATR_COUNT[almid] + 1$
$END$

$IF LENGTH(ALM.ACTION.CALLBACK.ID_LIST)$
	$ERROR ALM.ACTION.CALLBACK.TEXT_LINE[1]$$FORMAT(_("OsAlarmCallback can't be used in SC3"))$$END$
$END$

$ ALM.ACTION.*を2つ以上定義していないか
$FOREACH almid ALM.ID_LIST$
	$IF ALM.ALMATR_COUNT[almid] != 1$
		$FATAL = 1$
		$ERROR ALM.TEXT_LINE[almid]$$FORMAT(_("OsAlarmAction of OsAlarm(%1%) have too many sub container"), almid)$$END$
	$END$
$END$


$ =====================================================================
$ SCHTBLに関する処理
$ =====================================================================

$TRACE("ASSOCIATE SCHTBL")$

$ 省略された場合はNONEにする
$FOREACH schtblid SCHTBL.ID_LIST$
	$SCHTBL.SYNCSTRATEGY[schtblid] = "NONE"$
$END$

$ SCHTBL.AUTOSTART.*からSCHTBL.*にコピー
$FOREACH aid SCHTBL.AUTOSTART.ID_LIST$
	$schtblid                                 = SCHTBL.AUTOSTART.PARENT[aid]$
	$SCHTBL.APP_LIST[schtblid]                = SCHTBL.AUTOSTART.APP_LIST[aid]$
	$SCHTBL.STARTTICK[schtblid]               = SCHTBL.AUTOSTART.STARTTICK[aid]$
	$SCHTBL.AUTOSTARTTYPE[schtblid]           = SCHTBL.AUTOSTART.TYPE[aid]$
$END$

$FOREACH syncid SCHTBL.SYNC.ID_LIST$
	$schtblid = SCHTBL.SYNC.PARENT[syncid]$
	$SCHTBL.SYNCSTRATEGY[schtblid] = SCHTBL.SYNC.STRATEGY[syncid]$
$END$


$ =====================================================================
$ EXPPTACTに関する処理
$ =====================================================================

$ SCHTBL.EXPPT.ACTIVATETASK.*からEXPPTACTにコピー
$i = 0$
$FOREACH acttskid SCHTBL.EXPPT.ACTIVATETASK.ID_LIST$
	$expid                          = SCHTBL.EXPPT.ACTIVATETASK.PARENT[acttskid]$
	$schtblid                       = SCHTBL.EXPPT.PARENT[expid]$

	$EXPPTACT.SCHTBLID[i]           = schtblid$
	$EXPPTACT.OFFSET[i]             = SCHTBL.EXPPT.OFFSET[expid]$
	$EXPPTACT.TSKID[i]              = SCHTBL.EXPPT.ACTIVATETASK.TSKID[acttskid]$
	$EXPPTACT.EXPIREATR[i]          = "ACTIVATETASK"$

	$EXPPTACT.ID_LIST               = APPEND(EXPPTACT.ID_LIST, i)$
	$i                              = i + 1$
$END$

$ SCHTBL.EXPPT.SETEVENT.*からEXPPTACTにコピー
$FOREACH setevid SCHTBL.EXPPT.SETEVENT.ID_LIST$
	$expid                          = SCHTBL.EXPPT.SETEVENT.PARENT[setevid]$
	$schtblid                       = SCHTBL.EXPPT.PARENT[expid]$

	$EXPPTACT.SCHTBLID[i]           = schtblid$
	$EXPPTACT.OFFSET[i]             = SCHTBL.EXPPT.OFFSET[expid]$
	$EXPPTACT.TSKID[i]              = SCHTBL.EXPPT.SETEVENT.TSKID[setevid]$
	$EXPPTACT.EVTID[i]              = SCHTBL.EXPPT.SETEVENT.EVTID[setevid]$
	$EXPPTACT.EXPIREATR[i]          = "SETEVENT"$

	$EXPPTACT.ID_LIST               = APPEND(EXPPTACT.ID_LIST, i)$
	$i                              = i + 1$
$END$

$IF FATAL$
	$DIE()$
$END$
