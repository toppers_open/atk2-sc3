$
$  TOPPERS ATK2
$      Toyohashi Open Platform for Embedded Real-Time Systems
$      Automotive Kernel Version 2
$
$  Copyright (C) 2011-2013 by Center for Embedded Computing Systems
$              Graduate School of Information Science, Nagoya Univ., JAPAN
$  Copyright (C) 2011-2013 by FUJISOFT INCORPORATED, JAPAN
$  Copyright (C) 2011-2013 by FUJITSU VLSI LIMITED, JAPAN
$  Copyright (C) 2011-2013 by NEC Communication Systems, Ltd., JAPAN
$  Copyright (C) 2011-2013 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
$  Copyright (C) 2011-2013 by Renesas Electronics Corporation, JAPAN
$  Copyright (C) 2011-2013 by Sunny Giken Inc., JAPAN
$  Copyright (C) 2011-2013 by TOSHIBA CORPORATION, JAPAN
$  Copyright (C) 2011-2013 by Witz Corporation, JAPAN
$
$  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
$  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
$  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
$  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
$      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
$      スコード中に含まれていること．
$  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
$      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
$      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
$      の無保証規定を掲載すること．
$  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
$      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
$      と．
$    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
$        作権表示，この利用条件および下記の無保証規定を掲載すること．
$    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
$        報告すること．
$  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
$      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
$      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
$      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
$      免責すること．
$
$  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
$  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
$  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
$  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
$  の責任を負わない．
$
$  $Id: kernel_sapi.tf 6837 2013-01-23 10:46:48Z fsi-dankei $
$

$ =====================================================================
$ kernel.tfの処理の前に処理されるファイル
$ 静的APIのデータ構造からXMLのデータ構造に変換する処理
$ =====================================================================

$TRACE("EXEC kernel_sapi.tf")$

$ kernel.tfを実行できないようなエラーが発生したか
$FATAL = 0$


$ =====================================================================
$ DEF_OSAPのエラーチェックと関連づけ
$ =====================================================================

$FOREACH osapid OSAP.ID_LIST$
$	// KERNEL_DOMAINの使用禁止
$	// DOMAINの代わりにKERNEL_DOMAINを使うとDOMAINがTDOM_KERNELになるが，
$	// DOMAIN.ID_LISTに登録されない不具合があるため，OSAP.DOMAINでチェックする
	$IF EQ(OSAP.DOMAIN[osapid], "TDOM_KERNEL")$
		$ERROR OSAP.TEXT_LINE[osapid]$$FORMAT(_("No Use TDOM_KERNEL DOMAIN"))$$END$
	$END$
$END$

$FOREACH osapid OSAP.ID_LIST$
$	// OSAP.ID_LISTにはOSAP名が入っていないので，OSAP名が入ったID_LISTにする
	$new_id_list = APPEND(new_id_list, VALUE(OSAP.DOMAIN[osapid], osapid))$

$	// DOMAIN外に定義されていないか？
	$IF EQ(OSAP.DOMAIN[osapid], "")$
		$ERROR OSAP.TEXT_LINE[osapid]$$FORMAT(_("DEF_OSAP is out of DOMAIN"))$$END$
	$END$

$	// Restarttaskは存在するか？
	$IF !EQ(OSAP.RESTARTTASK[osapid], "NO_RESTART_TASK")$
		$FOREACH tskid TSK.ID_LIST$
			$IF EQ(OSAP.RESTARTTASK[osapid], tskid)$
				$OSAP.RESTARTTSKID[osapid] = tskid$
			$END$
		$END$
		$IF !LENGTH(OSAP.RESTARTTSKID[osapid])$
			$ERROR OSAP.TEXT_LINE[osapid]$$FORMAT(_("DEF_OSAP RestartTask `%1%\' no exist"), OSAP.RESTARTTASK[osapid])$$END$
		$END$
	$END$
$END$
$OSAP.ID_LIST = new_id_list$


$ =====================================================================
$ DOMAINのエラーチェック
$ =====================================================================

$FOREACH domid DOM.ID_LIST$
	$osap_cnt = 0$
	$FOREACH osapid OSAP.ID_LIST$
		$IF EQ(osapid, domid)$
			$osap_cnt = osap_cnt + 1$
		$END$
	$END$
$	// DOMAIN内にDEF_OSAPが存在するか?
	$IF osap_cnt == 0$
		$ERROR$$FORMAT(_("DOMAIN(%1%) no have DEF_OSAP "), domid)$$END$
	$END$
$	// DOMAIN内にDEF_OSAPが複数存在するか?
	$IF osap_cnt > 1$
		$ERROR$$FORMAT(_("DOMAIN(%1%) have too DEF_OSAP "), domid)$$END$
	$END$
$END$


$ =====================================================================
$ CRE_TFNのエラーチェックと関連づけ
$ =====================================================================

$FOREACH tfnid TFN.ID_LIST$
$	// DOMAIN内に記述されているかの確認
	$FOREACH osapid OSAP.ID_LIST$
		$IF EQ(TFN.DOMAIN[tfnid], osapid)$
			$TFN.OSAPID[tfnid] = osapid$
		$END$
	$END$

	$IF !LENGTH(TFN.OSAPID[tfnid])$
		$ERROR TFN.TEXT_LINE[tfnid]$$FORMAT(_("CRE_TFN Out of OSApplication"))$$END$
		$FATAL = 1$
	$END$
$END$


$ =====================================================================
$ ATT_REGのエラーチェック
$ =====================================================================

$FOREACH reg REG.ID_LIST$
$	// 保護ドメインに所属している場合
	$IF LENGTH(REG.DOMAIN[reg])$
		$ERROR REG.TEXT_LINE[reg]$$FORMAT(_("%1% belongs to a DOMAIN"), "ATT_REG")$$END$
	$END$
$END$


$ =====================================================================
$ ATT_MODの関連づけ
$ =====================================================================

$FOREACH modid MOD.ID_LIST$
	$FOREACH osapid OSAP.ID_LIST$
		$IF EQ(MOD.DOMAIN[modid], osapid)$
			$MOD.OSAPID[modid] = osapid$
		$END$
	$END$
$END$


$ =====================================================================
$ ATT_SECの関連づけ
$ =====================================================================

$FOREACH secid SEC.ID_LIST$
	$FOREACH osapid OSAP.ID_LIST$
		$IF EQ(SEC.DOMAIN[secid], osapid)$
			$SEC.OSAPID[secid] = osapid$
		$END$
	$END$

	$IF !LENGTH(SEC.MEMATR[secid])$
$		// 保護ドメインに所属している場合
		$IF LENGTH(SEC.DOMAIN[secid])$
			$ERROR SEC.TEXT_LINE[secid]$$FORMAT(_("%1% belongs to a DOMAIN"), "LNK_SEC")$$END$
		$END$
	$END$
$END$


$ =====================================================================
$ ATT_MEMの関連づけ
$ =====================================================================

$FOREACH memid MEM.ID_LIST$
	$FOREACH osapid OSAP.ID_LIST$
		$IF EQ(MEM.DOMAIN[memid], osapid)$
			$MEM.OSAPID[memid] = osapid$
		$END$
	$END$
$END$


$ =====================================================================
$ DEF_OSのエラーチェック
$ =====================================================================

$ 1個定義されているか．
$IF !LENGTH(OS.ID_LIST)$
	$FATAL = 1$
	$ERROR$$FORMAT(_("no DEF_OS is registered"))$$END$
$END$

$ DEF_OS
$ 2個以上定義されていないか．
$IF LENGTH(OS.ID_LIST) > 1$
	$ERROR OS.TEXT_LINE[1]$$FORMAT(_("DEF_OS is duplicated"))$$END$
$END$

$ 保護ドメインに所属している場合
$IF LENGTH(OS.DOMAIN[1])$
	$ERROR OS.TEXT_LINE[1]$$FORMAT(_("%1% belongs to a DOMAIN"), "DEF_OS")$$END$
$END$

$IF EQ(OS.SC[1], "AUTO")$
	$OS.SC[1] = {}$
$END$

$IF EQ(OS.MLEVEL[1], "AUTO")$
	$OS.MLEVEL[1] = {}$
$END$

$IF EQ(OS.VLEVEL[1], "AUTO")$
	$OS.VLEVEL[1] = {}$
$END$

$ =====================================================================
$ DEF_HOOKのエラーチェック
$ =====================================================================

$ 1個定義されているか．
$IF !LENGTH(HOOK.ID_LIST)$
	$FATAL = 1$
	$ERROR$$FORMAT(_("no DEF_HOOK is registered"))$$END$
$END$

$ 2個以上定義されていないか．
$IF LENGTH(HOOK.ID_LIST) > 1$
	$ERROR HOOK.TEXT_LINE[1]$$FORMAT(_("DEF_HOOK is duplicated"))$$END$
$END$

$ 保護ドメインに所属している場合
$IF LENGTH(HOOK.DOMAIN[1])$
	$ERROR HOOK.TEXT_LINE[1]$$FORMAT(_("%1% belongs to a DOMAIN"), "DEF_HOOK")$$END$
$END$


$ =====================================================================
$ DEF_OS_STKのエラーチェック
$ =====================================================================

$ 静的API「DEF_OS_STK」が複数ある
$IF LENGTH(OSTK.ID_LIST) > 1$
	$ERROR OSTK.TEXT_LINE[1]$$FORMAT(_("too many %1%"), "DEF_OS_STK")$$END$
$END$

$ 保護ドメインに所属している場合
$IF LENGTH(OSTK.DOMAIN[1])$
	$ERROR OSTK.TEXT_LINE[1]$$FORMAT(_("%1% belongs to a DOMAIN"), "DEF_OS_STK")$$END$
$END$


$ =====================================================================
$ DEF_HOOK_STKのエラーチェック
$ =====================================================================

$ 静的API「DEF_HOOK_STK」が複数ある
$IF LENGTH(HSTK.ID_LIST) > 1$
	$ERROR HSTK.TEXT_LINE[1]$$FORMAT(_("too many %1%"), "DEF_HOOK_STK")$$END$
$END$

$ 保護ドメインに所属している場合
$IF LENGTH(HSTK.DOMAIN[1])$
	$ERROR HSTK.TEXT_LINE[1]$$FORMAT(_("%1% belongs to a DOMAIN"), "DEF_HOOK_STK")$$END$
$END$

$ 先頭アドレスがNULLでない場合
$FOREACH hsstkid HSTK.ID_LIST$
	$IF !EQ(HSTK.STK[hsstkid], "NULL")$
		$ERROR HSTK.TEXT_LINE[hsstkid]$$FORMAT(_("STK of DEF_HOOK_STK must be NULL"))$$END$
	$END$
$END$


$ =====================================================================
$ DEF_NTHOOK_STKのエラーチェック
$ =====================================================================

$ 静的API「DEF_NTHOOK_STK」が複数ある
$IF LENGTH(NTHSTK.ID_LIST) > 1$
	$ERROR NTHSTK.TEXT_LINE[1]$$FORMAT(_("too many %1%"), "DEF_NTHOOK_STK")$$END$
$END$

$ 保護ドメインに所属している場合
$IF LENGTH(NTHSTK.DOMAIN[1])$
	$ERROR NTHSTK.TEXT_LINE[1]$$FORMAT(_("%1% belongs to a DOMAIN"), "DEF_NTHOOK_STK")$$END$
$END$


$ =====================================================================
$ CRE_APPのエラーチェック
$ =====================================================================

$ アプリケーションモードが最低1個は定義されているか．
$IF !LENGTH(APP.ID_LIST)$
	$ERROR$$FORMAT(_("no CRE_APP is registered"))$$END$
$END$

$FOREACH appid APP.ID_LIST$
$	// 保護ドメインに所属している場合
	$IF LENGTH(APP.DOMAIN[appid])$
		$ERROR APP.TEXT_LINE[appid]$$FORMAT(_("%1% belongs to a DOMAIN"), "CRE_APP")$$END$
	$END$
$END$


$ =====================================================================
$ CRE_EVTのエラーチェック
$ =====================================================================

$FOREACH evtid EVT.ID_LIST$
$	// 保護ドメインに所属している場合
	$IF LENGTH(EVT.DOMAIN[evtid])$
		$ERROR EVT.TEXT_LINE[evtid]$$FORMAT(_("%1% belongs to a DOMAIN"), "CRE_EVT")$$END$
	$END$
$END$


$ =====================================================================
$ CRE_TSKのエラーチェックと関連づけ
$ =====================================================================

$TRACE("ASSOCIATE TASK")$

$FOREACH tskid TSK.ID_LIST$
	$new_list = {}$
	$FOREACH evt TSK.EVT_LIST[tskid]$
		$find = 0$
		$FOREACH evtid EVT.ID_LIST$
			$IF EQ(evt, evtid)$
				$new_list = APPEND(new_list, evtid)$
				$find = 1$
			$END$
		$END$
$		// eventが存在するか．
		$IF !find$
			$ERROR TSK.TEXT_LINE[tskid]$$FORMAT(_("illegal %1% `%2%\'"), "event", evt)$$END$
		$END$
	$END$
	$TSK.EVT_LIST[tskid] = new_list$

	$new_list = {}$
	$FOREACH res TSK.RES_LIST[tskid]$
		$find = 0$
		$FOREACH resid RES.ID_LIST$
			$IF EQ(res, resid)$
				$new_list = APPEND(new_list, resid)$
				$find = 1$
			$END$
		$END$
$		// resourceが存在するか．
		$IF !find$
			$ERROR TSK.TEXT_LINE[tskid]$$FORMAT(_("illegal %1% `%2%\'"), "resource", res)$$END$
		$END$
	$END$
	$TSK.RES_LIST[tskid] = new_list$

	$new_list = {}$
	$FOREACH app TSK.APP_LIST[tskid]$
		$find = 0$
		$FOREACH appid APP.ID_LIST$
			$IF EQ(app, appid)$
				$new_list = APPEND(new_list, appid)$
				$find = 1$
			$END$
		$END$
$		// applicationが存在するか．
		$IF !find$
			$ERROR TSK.TEXT_LINE[tskid]$$FORMAT(_("illegal %1% `%2%\'"), "application", app)$$END$
		$END$
	$END$
	$TSK.APP_LIST[tskid] = new_list$

	$new_list = {}$
	$FOREACH osap TSK.ACS_OSAP_LIST[tskid]$
		$find = 0$
		$FOREACH osapid OSAP.ID_LIST$
			$IF EQ(osap, osapid)$
				$new_list = APPEND(new_list, osapid)$
				$find = 1$
			$END$
		$END$
$		// osapが存在するか．
		$IF !find$
			$ERROR TSK.TEXT_LINE[tskid]$$FORMAT(_("illegal %1% `%2%\'"), "OSApplication", osap)$$END$
		$END$
	$END$
	$TSK.ACS_OSAP_LIST[tskid] = new_list$

$	// DOMAIN内に記述されているかの確認
	$FOREACH osapid OSAP.ID_LIST$
		$IF EQ(TSK.DOMAIN[tskid], osapid)$
			$TSK.OSAPID[tskid] = osapid$
		$END$
	$END$

	$osapid = TSK.OSAPID[tskid]$
	$IF LENGTH(osapid)$
		$OSAP.TSK_LIST[osapid] = APPEND(OSAP.TSK_LIST[osapid], tskid)$
	$END$
$END$


$ =====================================================================
$ CRE_RESのエラーチェックと関連づけ
$ =====================================================================

$FOREACH resid RES.ID_LIST$

$	// DOMAIN外に定義されているか？
	$IF !EQ(RES.DOMAIN[resid], "")$
		$ERROR RES.TEXT_LINE[resid]$$FORMAT(_("CRE_RES is within a DOMAIN"))$$END$
	$END$

	$IF EQ(RES.RESATR[resid], "LINKED")$
		$FOREACH resid2 RES.ID_LIST$
			$IF EQ(resid2, RES.LINKEDRESOURCE[resid])$
				$RES.LINKEDRESID[resid] = resid2$
			$END$
		$END$

$		// 被リンク側リソースが存在するか.
		$IF !LENGTH(RES.LINKEDRESID[resid])$
			$ERROR RES.TEXT_LINE[resid]$$FORMAT(_("illegal %1% `%2%\'"), "std_resource", RES.LINKEDRESOURCE[resid])$$END$
			$FATAL = 1$
		$END$
	$END$

	$new_list = {}$
	$FOREACH osap RES.ACS_OSAP_LIST[resid]$
		$find = 0$
		$FOREACH osapid OSAP.ID_LIST$
			$IF EQ(osap, osapid)$
				$new_list = APPEND(new_list, osapid)$
				$find = 1$
			$END$
		$END$
$		// osapが存在するか．
		$IF !find$
			$ERROR RES.TEXT_LINE[resid]$$FORMAT(_("illegal %1% `%2%\'"), "OSApplication", osap)$$END$
		$END$
	$END$
	$RES.ACS_OSAP_LIST[resid] = new_list$
$END$


$ =====================================================================
$ CRE_ISRのエラーチェックと関連づけ
$ =====================================================================

$TRACE("ASSOCIATE ISR")$

$FOREACH isrid ISR.ID_LIST$
$	// intatrにENABLEが含まれていない場合
	$IF !(ISR.INTATR[isrid] & ENABLE) && EQ(ISR.CATEGORY[isrid], "CATEGORY_1")$
		$ERROR ISR.TEXT_LINE[isrid]$$FORMAT(_("Category 1 ISR %1%'s intatr must include ENABLE"), isrid)$$END$
	$END$

	$new_list = {}$
	$FOREACH res ISR.RES_LIST[isrid]$
		$find = 0$
		$FOREACH resid RES.ID_LIST$
			$IF EQ(res, resid)$
				$new_list = APPEND(new_list, resid)$
				$find = 1$
			$END$
		$END$
$		// resourceが存在するか．
		$IF !find$
			$ERROR ISR.TEXT_LINE[isrid]$$FORMAT(_("illegal %1% `%2%\'"), "resource", res)$$END$
		$END$
	$END$
	$ISR.RES_LIST[isrid] = new_list$

	$new_list = {}$
	$FOREACH osap ISR.ACS_OSAP_LIST[isrid]$
		$find = 0$
		$FOREACH osapid OSAP.ID_LIST$
			$IF EQ(osap, osapid)$
				$new_list = APPEND(new_list, osapid)$
				$find = 1$
			$END$
		$END$
$		// osapが存在するか．
		$IF !find$
			$ERROR ISR.TEXT_LINE[isrid]$$FORMAT(_("illegal %1% `%2%\'"), "OSApplication", osap)$$END$
		$END$
	$END$
	$ISR.ACS_OSAP_LIST[isrid] = new_list$

$ 	// DOMAINの囲み内に記述されているかをチェック
	$FOREACH osapid OSAP.ID_LIST$
		$IF EQ(ISR.DOMAIN[isrid], osapid)$
			$ISR.OSAPID[isrid] = osapid$
		$END$
	$END$

	$osapid = ISR.OSAPID[isrid]$
	$IF LENGTH(osapid)$
		$OSAP.ISR_LIST[osapid] = APPEND(OSAP.ISR_LIST[osapid], isrid)$
	$END$
$END$


$ =====================================================================
$ CRE_ALMのエラーチェックと関連付け
$ =====================================================================

$TRACE("ASSOCIATE ALARM")$

$FOREACH almid ALM.ID_LIST$
	$almatr = ALM.ALMATR[almid]$
$	// almatrにACTIVATETASK/SETEVENT/CALLBACK/INCREMENTCOUNTER以外のものが登録されていないか
	$IF !(EQ(almatr, "ACTIVATETASK") || EQ(almatr, "SETEVENT") || EQ(almatr, "CALLBACK") || EQ(almatr, "INCREMENTCOUNTER"))$
		$ERROR ALM.TEXT_LINE[almid]$$FORMAT(_("illegal %1% `%2%\'"), "almatr", almatr)$$END$
	$END$

	$FOREACH cntid CNT.ID_LIST$
$		// ALMとCNTの関連づけ
		$IF EQ(cntid, ALM.COUNTER[almid])$
			$ALM.CNTID[almid] = cntid$
		$END$
	$END$

$	// counterが存在するか
	$IF !LENGTH(ALM.CNTID[almid])$
		$FATAL = 1$
		$ERROR ALM.TEXT_LINE[almid]$$FORMAT(_("illegal %1% `%2%\'"), "counter", ALM.COUNTER[almid])$$END$
	$END$

	$almatr = ALM.ALMATR[almid]$
	$IF EQ(almatr, "ACTIVATETASK") || EQ(almatr, "SETEVENT")$
$		// ALMとTSKの関連づけ
		$FOREACH tskid TSK.ID_LIST$
			$IF EQ(tskid, ALM.TASK[almid])$
				$ALM.TSKID[almid] = tskid$
			$END$
		$END$

$		// taskが存在するか
		$IF !LENGTH(ALM.TSKID[almid])$
			$FATAL = 1$
			$ERROR ALM.TEXT_LINE[almid]$$FORMAT(_("illegal %1% `%2%\'"), "task", ALM.TASK[almid])$$END$
		$END$

		$IF EQ(almatr, "SETEVENT")$
$			// ALMとEVTの関連づけ
			$FOREACH evtid EVT.ID_LIST$
				$IF EQ(evtid, ALM.EVENT[almid])$
					$ALM.EVTID[almid] = evtid$
				$END$
			$END$

$			// eventが存在するか
			$IF !LENGTH(ALM.EVTID[almid])$
				$FATAL = 1$
				$ERROR ALM.TEXT_LINE[almid]$$FORMAT(_("illegal %1% `%2%\'"), "event", ALM.EVENT[almid])$$END$
			$END$
		$END$
	$END$

	$IF EQ(almatr, "INCREMENTCOUNTER")$
		$FOREACH cntid CNT.ID_LIST$
$			// ALMとIncrementCounterの関連づけ
			$IF EQ(cntid, ALM.INCOUNTER[almid])$
				$ALM.INCID[almid] = cntid$
			$END$
		$END$

$		// incounterが存在するか
		$IF !LENGTH(ALM.INCID[almid])$
			$FATAL = 1$
			$ERROR ALM.TEXT_LINE[almid]$$FORMAT(_("nothing %1% `%2%\'"), "incounter", ALM.INCOUNTER[almid])$$END$
		$END$
	$END$

	$new_list = {}$
	$FOREACH app ALM.APP_LIST[almid]$
		$find = 0$
		$FOREACH appid APP.ID_LIST$
			$IF EQ(app, appid)$
				$new_list = APPEND(new_list, appid)$
				$find = 1$
			$END$
		$END$
$		// applicationが存在するか．
		$IF !find$
			$ERROR ALM.TEXT_LINE[almid]$$FORMAT(_("illegal %1% `%2%\'"), "application", app)$$END$
		$END$
	$END$
	$ALM.APP_LIST[almid] = new_list$

	$new_list = {}$
	$FOREACH osap ALM.ACS_OSAP_LIST[almid]$
		$find = 0$
		$FOREACH osapid OSAP.ID_LIST$
			$IF EQ(osap, osapid)$
				$new_list = APPEND(new_list, osapid)$
				$find = 1$
			$END$
		$END$
$		// osapが存在するか．
		$IF !find$
			$ERROR ALM.TEXT_LINE[almid]$$FORMAT(_("illegal %1% `%2%\'"), "OSApplication", osap)$$END$
		$END$
	$END$
	$ALM.ACS_OSAP_LIST[almid] = new_list$

$	// DOMAINの囲み内に記述されているかをチェック
	$FOREACH osapid OSAP.ID_LIST$
		$IF EQ(ALM.DOMAIN[almid], osapid)$
			$ALM.OSAPID[almid] = osapid$
		$END$
	$END$

	$osapid = ALM.OSAPID[almid]$
	$IF LENGTH(osapid)$
		$OSAP.ALM_LIST[osapid] = APPEND(OSAP.ALM_LIST[osapid], almid)$
	$END$
$END$


$ =====================================================================
$ CNTのエラーチェックと関連づけ
$ =====================================================================

$TRACE("ASSOCIATE COUNTER")$

$FOREACH cntid CNT.ID_LIST$
	$IF EQ(CNT.CNTATR[cntid], "HARDWARE")$
		$FOREACH isrid ISR.ID_LIST$
			$IF EQ(isrid, CNT.ISR[cntid])$
				$CNT.ISRID[cntid] = isrid$
			$END$
		$END$

		$IF !LENGTH(CNT.ISRID[cntid])$
			$FATAL = 1$
			$ERROR CNT.TEXT_LINE[cntid]$$FORMAT(_("hardware counter %1%'s ISR is not defined in CRE_ISR"), cntid)$$END$
		$END$

	$END$

	$new_list = {}$
	$FOREACH osap CNT.ACS_OSAP_LIST[cntid]$
		$find = 0$
		$FOREACH osapid OSAP.ID_LIST$
			$IF EQ(osap, osapid)$
				$new_list = APPEND(new_list, osapid)$
				$find = 1$
			$END$
		$END$
$		// osapが存在するか．
		$IF !find$
			$ERROR CNT.TEXT_LINE[cntid]$$FORMAT(_("illegal %1% `%2%\'"), "OSApplication", osap)$$END$
		$END$
	$END$
	$CNT.ACS_OSAP_LIST[cntid] = new_list$

$	// DOMAIN内に記述されているかの確認
	$FOREACH osapid OSAP.ID_LIST$
		$IF EQ(CNT.DOMAIN[cntid], osapid)$
			$CNT.OSAPID[cntid] = osapid$
		$END$
	$END$

	$osapid = CNT.OSAPID[cntid]$
	$IF LENGTH(osapid)$
		$OSAP.CNT_LIST[osapid] = APPEND(OSAP.CNT_LIST[osapid], cntid)$
	$END$
$END$


$ =====================================================================
$ CRE_SCHTBLのエラーチェックと関連付け
$ =====================================================================

$TRACE("ASSOCIATE SCHEDULETABLE")$

$FOREACH schtblid SCHTBL.ID_LIST$
$	// SCHTBLとCNTの関連づけ
	$FOREACH cntid CNT.ID_LIST$
		$IF EQ(cntid, SCHTBL.COUNTER[schtblid])$
			$SCHTBL.CNTID[schtblid] = cntid$
		$END$
	$END$

$	// 駆動カウンタは存在するか
	$IF !LENGTH(SCHTBL.CNTID[schtblid])$
		$FATAL = 1$
		$ERROR SCHTBL.TEXT_LINE[schtblid]$$FORMAT(_("schedule table `%1%\' shall be driven by one counter"), schtblid)$$END$
	$END$

	$new_list = {}$
	$FOREACH app SCHTBL.APP_LIST[schtblid]$
		$find = 0$
		$FOREACH appid APP.ID_LIST$
			$IF EQ(app, appid)$
				$new_list = APPEND(new_list, appid)$
				$find = 1$
			$END$
		$END$
$		// applicationが存在するか．
		$IF !find$
			$ERROR SCHTBL.TEXT_LINE[schtblid]$$FORMAT(_("illegal %1% `%2%\'"), "application", app)$$END$
		$END$
	$END$
	$SCHTBL.APP_LIST[schtblid] = new_list$

	$new_list = {}$
	$FOREACH osap SCHTBL.ACS_OSAP_LIST[schtblid]$
		$find = 0$
		$FOREACH osapid OSAP.ID_LIST$
			$IF EQ(osap, osapid)$
				$new_list = APPEND(new_list, osapid)$
				$find = 1$
			$END$
		$END$
$		// osapが存在するか．
		$IF !find$
			$ERROR SCHTBL.TEXT_LINE[schtblid]$$FORMAT(_("illegal %1% `%2%\'"), "OSApplication", osap)$$END$
		$END$
	$END$
	$SCHTBL.ACS_OSAP_LIST[schtblid] = new_list$

$ 	// DOMAINの囲み内に記述されているかをチェック
	$FOREACH osapid OSAP.ID_LIST$
		$IF EQ(SCHTBL.DOMAIN[schtblid], osapid)$
			$SCHTBL.OSAPID[schtblid] = osapid$
		$END$
	$END$

	$osapid = SCHTBL.OSAPID[schtblid]$
	$IF LENGTH(osapid)$
		$OSAP.SCHTBL_LIST[osapid] = APPEND(OSAP.SCHTBL_LIST[osapid], schtblid)$
	$END$
$END$


$ =====================================================================
$ DEF_EXPIREPTのエラーチェックと関連付け
$ =====================================================================

$FOREACH expptactid EXPPTACT.ID_LIST$
	$IF !(EQ(EXPPTACT.EXPIREATR[expptactid], "ACTIVATETASK") || EQ(EXPPTACT.EXPIREATR[expptactid], "SETEVENT"))$
		$FATAL = 1$
		$ERROR EXPPTACT.TEXT_LINE[expptactid]$$FORMAT(_("illegal %1% `%2%\' in %3%"), "expireatr", EXPPTACT.EXPIREATR[expptactid], "DEF_EXPIREPT")$$END$
	$END$

$	// SCHTBLとEXPPTACTの関連づけ
	$FOREACH schtblid SCHTBL.ID_LIST$
		$IF EQ(EXPPTACT.SCHEDULETABLE[expptactid], schtblid)$
			$EXPPTACT.SCHTBLID[expptactid] = schtblid$
		$END$
	$END$

	$schtblid = EXPPTACT.SCHTBLID[expptactid]$

	$IF LENGTH(schtblid)$
		$IF !EQ(SCHTBL.DOMAIN[schtblid], EXPPTACT.DOMAIN[expptactid])$
			$ERROR EXPPTACT.TEXT_LINE[expptactid]$$FORMAT(_("%1% and %2% must belong to same DOMAIN"), "DEF_EXPIREPT", "CRE_SCHTBL")$$END$
		$END$
	$ELSE$
$		// スケジュールテーブルに属さないアクション
		$ERROR EXPPTACT.TEXT_LINE[expptactid]$$FORMAT(_("scheduletable(%1%) is not defined"), EXPPTACT.SCHEDULETABLE[expptactid])$$END$
	$END$

$	// EXPPTACTとTSKとの関連づけ
	$FOREACH tskid TSK.ID_LIST$
		$IF EQ(tskid, EXPPTACT.TASK[expptactid])$
			$EXPPTACT.TSKID[expptactid] = tskid$
		$END$
	$END$

$	// taskが存在するか
	$IF !LENGTH(EXPPTACT.TSKID[expptactid])$
		$FATAL = 1$
		$ERROR EXPPTACT.TEXT_LINE[expptactid]$$FORMAT(_("illegal %1% `%2%\' in %3%"), "task", EXPPTACT.TASK[expptactid], "DEF_EXPIREPT")$$END$
	$END$

	$IF EQ(EXPPTACT.EXPIREATR[expptactid], "SETEVENT")$
$		// EXPPTACTとEVTとの関連づけ
		$FOREACH evtid EVT.ID_LIST$
			$IF EQ(evtid, EXPPTACT.EVENT[expptactid])$
				$EXPPTACT.EVTID[expptactid] = evtid$
			$END$
		$END$

$		// eventが存在するか
		$IF !LENGTH(EXPPTACT.EVTID[expptactid])$
			$FATAL = 1$
			$ERROR EXPPTACT.TEXT_LINE[expptactid]$$FORMAT(_("illegal %1% `%2%\' in %3%"), "event", EXPPTACT.EVENT[expptactid], "DEF_EXPIREPT")$$END$
		$END$
	$END$
$END$

$	// SCHTBLにEXPPTACTが一つは存在するか
$FOREACH schtblid SCHTBL.ID_LIST$
	$find = 0$
	$FOREACH expptactid EXPPTACT.ID_LIST$
		$IF EQ(EXPPTACT.SCHTBLID[expptactid], schtblid)$
			$find = 1$
		$END$
	$END$

	$IF !find$
		$FATAL = 1$
		$ERROR SCHTBL.TEXT_LINE[schtblid]$$FORMAT(_("DEF_EXPIREPT for scheduletable(%1%) does not exist"), schtblid)$$END$
	$END$
$END$

$IF FATAL$
	$DIE()$
$END$
