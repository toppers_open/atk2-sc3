$
$  TOPPERS ATK2
$      Toyohashi Open Platform for Embedded Real-Time Systems
$      Automotive Kernel Version 2
$
$  Copyright (C) 2011-2013 by Center for Embedded Computing Systems
$              Graduate School of Information Science, Nagoya Univ., JAPAN
$  Copyright (C) 2011-2013 by FUJISOFT INCORPORATED, JAPAN
$  Copyright (C) 2011-2013 by FUJITSU VLSI LIMITED, JAPAN
$  Copyright (C) 2011-2013 by NEC Communication Systems, Ltd., JAPAN
$  Copyright (C) 2011-2013 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
$  Copyright (C) 2011-2013 by Renesas Electronics Corporation, JAPAN
$  Copyright (C) 2011-2013 by Sunny Giken Inc., JAPAN
$  Copyright (C) 2011-2013 by TOSHIBA CORPORATION, JAPAN
$  Copyright (C) 2011-2013 by Witz Corporation, JAPAN
$
$  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
$  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
$  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
$  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
$      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
$      スコード中に含まれていること．
$  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
$      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
$      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
$      の無保証規定を掲載すること．
$  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
$      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
$      と．
$    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
$        作権表示，この利用条件および下記の無保証規定を掲載すること．
$    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
$        報告すること．
$  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
$      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
$      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
$      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
$      免責すること．
$
$  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
$  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
$  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
$  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
$  の責任を負わない．
$
$  $Id: kernel_mem2.tf 7284 2013-03-29 06:18:22Z fujitsu-wada $
$

$CFG_PASS4 = 1$
$
$  パス2からの情報の読込み
$
$INCLUDE "cfg2_out.tf"$
$INCLUDE "kernel/kernel_common.tf"$


$FILE "kernel_mem.c"$
/* kernel_mem.c */$NL$
#include "kernel/kernel_int.h"$NL$
#include "Os_Lcfg.h"$NL$
#include "kernel_mem.h"$NL$
#ifndef TOPPERS_EMPTY_LABEL$NL$
#define TOPPERS_EMPTY_LABEL(x, y) x y[0]$NL$
#endif$NL$
$NL$

/*$NL$
$SPC$*  Include Directives (#include)$NL$
$SPC$*/$NL$
$NL$
$INCLUDES$
$NL$

$
$  メモリオブジェクトのサイズをチェック
$
$FUNCTION CHECK_MO_ADDR$
	$FOREACH moid MO_SECTION_LIST$
		$IF MO.BASEADDR[moid] > MO.LIMITADDR[moid] && MO.LIMITADDR[moid] != 0$
			$IF MO.TYPE[moid] == TOPPERS_ATTMEM$
				$ERROR MO.TEXT_LINE[moid]$
					$FORMAT(_("%1% `%2%\' is too large"),
								"size", MO.SIZE[moid])$
				$END$
			$ELSE$
				$ERROR MO.TEXT_LINE[moid]$
					$FORMAT(_("illegal memory object size"))$
				$END$
			$END$
		$END$
	$END$
$END$

$
$  メモリオブジェクトの重なりをチェック
$
$FUNCTION CHECK_MO_OVERLAP$
	$prev = 0$
	$FOREACH moid SORT(MO_SECTION_LIST, "MO.BASEADDR")$
		$IF MO.BASEADDR[moid] != MO.LIMITADDR[moid]$
			$IF !prev || (MO.LIMITADDR[prev] <= MO.BASEADDR[moid]
													&& MO.LIMITADDR[prev] != 0)$
				$prev = moid$
			$ELSE$
$				// メモリオブジェクトの領域に重なりがある場合
				$IF !(MO.TYPE[moid] == TOPPERS_USTACK && MO.TYPE[prev] == TOPPERS_USTACK)$
$					// エラーメッセージの出力
					$IF MO.TYPE[moid] == TOPPERS_ATTMEM$
						$ERROR MO.TEXT_LINE[moid]$
							$FORMAT(_("memory object overlaps with another memory object"))$
						$END$
					$ELIF MO.TYPE[prev] == TOPPERS_ATTMEM$
						$ERROR MO.TEXT_LINE[prev]$
							$FORMAT(_("memory object overlaps with another memory object"))$
						$END$
					$ELIF MO.TYPE[moid] == TOPPERS_USTACK$
						$ERROR MO.TEXT_LINE[moid]$
							$FORMAT(_("user stack area overlaps with another memory object"))$
						$END$
					$ELIF MO.TYPE[prev] == TOPPERS_USTACK$
						$ERROR MO.TEXT_LINE[prev]$
							$FORMAT(_("user stack area overlaps with another memory object"))$
						$END$
					$ELSE$
						$ERROR MO.TEXT_LINE[moid]$
							$FORMAT(_("memory objects overlap"))$
						$END$
					$END$
				$END$
			$END$
		$END$
	$END$
$END$

$
$  メモリ領域を含むメモリオブジェクトのサーチ
$
$FUNCTION SEARCH_MO$
	$_base = ARGV[1]$
	$_limit = (ARGV[1] + ARGV[2]) & ((1 << sizeof_void_ptr * 8) - 1)$
	$IF _limit < _base && _limit != 0$
		$RESULT = 0$
	$ELSE$
		$i = 1$
		$j = LENGTH(MO_MEMTOP_ORDER)$
		$found = 0$

		$WHILE !found && i <= j$
			$k = (i + j) / 2$
			$_moid = AT(MO_MEMTOP_ORDER,k-1)$
			$IF _base < MO.BASEADDR[_moid]$
				$j = k - 1$
			$ELIF _base >= MO.LIMITADDR[_moid] && MO.LIMITADDR[_moid] != 0$
				$i = k + 1$
			$ELSE$
				$found = _moid$
			$END$
		$END$
		$IF found && _limit > MO.LIMITADDR[found] && MO.LIMITADDR[found] != 0$
			$found = 0$
		$END$
		$RESULT = found$
	$END$
$END$

$
$  メモリ領域がカーネル専用のメモリオブジェクトに含まれているかのチェック
$
$FUNCTION CHECK_MEMOBJ_KERNEL$
	$moid = SEARCH_MO(ARGV[1], ARGV[2])$
	$IF moid && MO.ACPTN_R[moid] == TACP_KERNEL
			 && MO.ACPTN_W[moid] == TACP_KERNEL
			 && MO.ACPTN_X[moid] == TACP_KERNEL$
		$RESULT = 0$
	$ELSE$
		$RESULT = 1$
	$END$
$END$

$ MO.BASEADDR, MO.LIMITADDRの設定
$SET_MO_ADDR()$

$ MO_MEMTOP_ORDER_MEMINIBの作成
$SET_MEMTOP_ORDER()$

$ MO_MEMTOP_ORDER_MEMINIBの統合
$MERGE_MEMINIB()$

$tnum_meminib = PEEK(SYMBOL("tnum_meminib"), sizeof_void_ptr)$

$IF !OMIT_STANDARD_MEMINIB$
$ memtop_table, meminib_tableの出力
$GENERATE_MEMINIB_TABLE(tnum_meminib)$
$END$
$ データセクション初期化テーブルを出力する(kernel_common.tf)
$GENERATE_DATASECINIB_TABLE()$

$ BSSセクション初期化テーブルを出力する(kernel_common.tf)
$GENERATE_BSSSECINIB_TABLE()$

$ ターゲット依存部で必要なMPUINFOBを出力する
$GENERATE_TARGET_MPUINFOB()$

$ タスク初期化ブロックを出力する(kernel_common.tf)
$GENERATE_TINIB_TABLE()$

$ OSアプリケーション初期化ブロックを出力する(kernel_common.tf)
$GENERATE_OSAPINIB_TABLE()$


$ メモリオブジェクトのサイズをチェック
$CHECK_MO_ADDR()$

$ メモリオブジェクトの重なりをチェック
$CHECK_MO_OVERLAP()$

$ MO_MEMTOP_ORDER：メモリオブジェクトのベースアドレス順のリスト
$MO_MEMTOP_ORDER = SORT(MO_SECTION_LIST, "MO.BASEADDR")$

$
$ システムタスクのスタック領域とユーザタスクのシステムスタック領域が，
$ カーネル専用のメモリオブジェクトに含まれているかのチェック
$
$IF !USE_TSKINICTXB$
	$tinib = SYMBOL("tinib_table")$
	$FOREACH tskid TSK.ID_LIST$
		$sstk = PEEK(tinib + offsetof_TINIB_sstk, sizeof_void_ptr)$
		$sstksz = PEEK(tinib + offsetof_TINIB_sstksz, sizeof_MemorySizeType)$
		$IF CHECK_MEMOBJ_KERNEL(sstk, sstksz)$
			$IF OSAP.TRUSTED[TSK.OSAPID[tskid]]$
				$ERROR TSK.TEXT_LINE[tskid]$
					$FORMAT(_("stack area of `%1%\' is not included in any kernel memory object"), tskid)$
				$END$
			$ELSE$
				$ERROR TSK.TEXT_LINE[tskid]$
					$FORMAT(_("system stack area of `%1%\' is not included in any kernel memory object"), tskid)$
				$END$
			$END$
		$END$
		$tinib = tinib + sizeof_TINIB$
	$END$
$END$

$
$ 非タスクコンテキスト用のスタック領域が，カーネル専用のメモリオブジェ
$ クトに含まれているかのチェック
$
$ostk = PEEK(SYMBOL("_ostk"), sizeof_void_ptr)$
$ostksz = PEEK(SYMBOL("_ostksz"), sizeof_MemorySizeType)$
$IF CHECK_MEMOBJ_KERNEL(ostk, ostksz)$
	$ERROR OSTK.TEXT_LINE[1]$
		$FORMAT(_("OS stack area is not included in any kernel memory object"))$
	$END$
$END$

$FILE "kernel_mem.h"$
/* kernel_mem.h */$NL$
#ifndef TOPPERS_KERNEL_MEM_H$NL$
#define TOPPERS_KERNEL_MEM_H$NL$
/*$NL$
$SPC$*  Include Directives (#include)$NL$
$SPC$*/$NL$
$NL$
$INCLUDES$
$NL$
$NL$
$ セクションの開始，終端のラベルをextern宣言する
$EXTERN_SECTION_LABEL()$

$ タスクスタック領域をextern宣言する(kernel_common.tf)
$GENERATE_EXPORT_TSK_STK()$

$ タスクをextern宣言する(kernel_common.tf)
$EXTERN_TSK()$

#endif /* TOPPERS_KERNEL_MEM_H */$NL$

$INCLUDE "target_check.tf"$
