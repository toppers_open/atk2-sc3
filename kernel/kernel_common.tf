$
$  TOPPERS ATK2
$      Toyohashi Open Platform for Embedded Real-Time Systems
$      Automotive Kernel Version 2
$
$  Copyright (C) 2011-2013 by Center for Embedded Computing Systems
$              Graduate School of Information Science, Nagoya Univ., JAPAN
$  Copyright (C) 2011-2013 by FUJISOFT INCORPORATED, JAPAN
$  Copyright (C) 2011-2013 by FUJITSU VLSI LIMITED, JAPAN
$  Copyright (C) 2011-2013 by NEC Communication Systems, Ltd., JAPAN
$  Copyright (C) 2011-2013 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
$  Copyright (C) 2011-2013 by Renesas Electronics Corporation, JAPAN
$  Copyright (C) 2011-2013 by Sunny Giken Inc., JAPAN
$  Copyright (C) 2011-2013 by TOSHIBA CORPORATION, JAPAN
$  Copyright (C) 2011-2013 by Witz Corporation, JAPAN
$
$  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
$  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
$  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
$  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
$      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
$      スコード中に含まれていること．
$  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
$      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
$      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
$      の無保証規定を掲載すること．
$  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
$      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
$      と．
$    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
$        作権表示，この利用条件および下記の無保証規定を掲載すること．
$    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
$        報告すること．
$  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
$      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
$      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
$      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
$      免責すること．
$
$  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
$  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
$  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
$  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
$  の責任を負わない．
$
$  $Id: kernel_common.tf 7284 2013-03-29 06:18:22Z fujitsu-wada $
$

$IF LENGTH(CFG_XML)$
	$INCLUDE "kernel/kernel_xml_common.tf"$
$END$

$
$  データセクションのLMAからVMAへのコピー
$
$FOREACH lma LMA.ORDER_LIST$
	$start_data = SYMBOL(LMA.START_DATA[lma])$
	$end_data = SYMBOL(LMA.END_DATA[lma])$
	$start_idata = SYMBOL(LMA.START_IDATA[lma])$
	$IF !LENGTH(start_data)$
		$ERROR$$FORMAT(_("symbol '%1%' not found"), LMA.START_DATA[lma])$$END$
	$ELIF !LENGTH(end_data)$
		$ERROR$$FORMAT(_("symbol '%1%' not found"), LMA.END_DATA[lma])$$END$
	$ELIF !LENGTH(start_idata)$
		$ERROR$$FORMAT(_("symbol '%1%' not found"), LMA.START_IDATA[lma])$$END$
	$ELSE$
		$BCOPY(start_idata, start_data, end_data - start_data)$
	$END$
$END$

$
$  セクションの開始，終端のラベルをextern宣言する
$
$FUNCTION EXTERN_SECTION_LABEL$
	$FOREACH moid MO_SECTION_LIST$
		$IF MO.LINKER[moid]$
			extern uint8 $REGEX_REPLACE(MO.START[moid], "^&", "")$;$NL$
			extern uint8 $REGEX_REPLACE(MO.LIMIT[moid], "^&", "")$;$NL$
			extern uint8 $REGEX_REPLACE(MO.END[moid],   "^&", "")$;$NL$
		$END$
	$END$$NL$

	$IF !OMIT_IDATA$
		$FOREACH moid DATASEC_LIST$
			extern uint8 __start_$MO.ILABEL[moid]$;$NL$
		$END$$NL$
	$END$
$END$

$
$  タスクのスタックをextern宣言する
$
$FUNCTION GENERATE_EXPORT_TSK_STK$
	$FOREACH tskid TSK.ID_LIST$
		$IF OSAP.TRUSTED[TSK.OSAPID[tskid]]$
			$extern_sstk = EQ(TSK.STK[tskid], "NULL") && !LENGTH(TSK.SHARED_SSTK_ID[tskid])$
			$extern_ustk = 0$
		$ELSE$
			$extern_sstk = EQ(TSK.SSTK[tskid], "NULL") && !LENGTH(TSK.SHARED_SSTK_ID[tskid])$
			$extern_ustk = EQ(TSK.STK[tskid], "NULL") && !LENGTH(TSK.SHARED_USTK_ID[tskid])$
		$END$

		$IF extern_sstk$
			extern StackType _kernel_sstack_$tskid$[COUNT_STK_T($TSK.TINIB_SSTKSZ[tskid]$)];$NL$
		$END$
		$IF extern_ustk$
			extern StackType _kernel_ustack_$tskid$[COUNT_STK_T($TSK.TINIB_USTKSZ[tskid]$)];$NL$
		$END$
	$END$
	$FOREACH tskpri RANGE(TMIN_TPRI, TMAX_TPRI)$
		$IF LENGTH(shared_ustack_size[tskpri])$
			extern StackType _kernel_shared_ustack_$tskpri$[COUNT_STK_T(ROUND_STK_T($shared_ustack_size[tskpri]$))];$NL$
		$END$
		$IF LENGTH(shared_sstack_size[tskpri])$
			extern StackType _kernel_shared_sstack_$tskpri$[COUNT_STK_T(ROUND_STK_T($shared_sstack_size[tskpri]$))];$NL$
		$END$
	$END$
	$NL$
$END$

$
$ タスクのextern宣言
$
$FUNCTION EXTERN_TSK$
$ 	//タスクのextern宣言
	$FOREACH tskid TSK.ID_LIST$
		extern TASK($tskid$);$NL$
	$END$
	$NL$
$END$

$
$ タスク初期化ブロックの出力
$
$FUNCTION GENERATE_TINIB_TABLE$
	/* Task Initialization Block */$NL$
	$IF LENGTH(TSK.ID_LIST)$
		const TINIB tinib_table[TNUM_TASK] = {$NL$
		$JOINEACH tskid TSK.ID_LIST ",\n"$
			$TAB${$NL$
			$TAB$$TAB$&TASKNAME($tskid$),$NL$

$			// タスク初期化コンテキストブロック，スタック領域
			$IF USE_TSKINICTXB$
				$GENERATE_TSKINICTXB(tskid)$
			$ELSE$
				$TAB$$TAB$$TSK.TINIB_SSTKSZ[tskid]$,$NL$
				$TAB$$TAB$(void *) $TSK.TINIB_SSTK[tskid]$,$NL$
				$TAB$$TAB$$TSK.TINIB_USTKSZ[tskid]$,$NL$
				$TAB$$TAB$(void *) $TSK.TINIB_USTK[tskid]$,$NL$
			$END$

			$TAB$$TAB$&(osapinib_table[$TSK.OSAPID[tskid]$]),$NL$
			$TAB$$TAB$$FORMAT("0x%08xU", +TSK.ACSBTMP[tskid])$,$NL$
			$TAB$$TAB$$TMAX_TPRI - TSK.PRIORITY[tskid]$U,$NL$
			$IF EQ(TSK.SCHEDULE[tskid], "NON") $
				$TAB$$TAB$$+TPRI_MAXTASK$,$NL$
			$ELSE$
				$IF LENGTH(TSK.INRESPRI[tskid])$
					$TAB$$TAB$$TMAX_TPRI - TSK.INRESPRI[tskid]$,$NL$
				$ELSE$
					$TAB$$TAB$$TMAX_TPRI - TSK.PRIORITY[tskid]$,$NL$
				$END$
			$END$

			$TAB$$TAB$($TSK.ACTIVATION[tskid]$) - 1U,$NL$
			$IF !OMIT_STKMPUINFOB$
				$TAB$$TAB$$FORMAT("0x%08xU", +TSK.ASTPTN[tskid])$,$NL$
				$GENERATE_STKMPUINFOB(tskid)$
			$ELSE$
				$TAB$$TAB$$FORMAT("0x%08xU", +TSK.ASTPTN[tskid])$$NL$
			$END$
			$TAB$}
		$END$
		$NL$
		};$NL$
	$ELSE$
		TOPPERS_EMPTY_LABEL(const TINIB, tinib_table);$NL$
	$END$
	$NL$
$END$

$
$  データセクション初期化テーブルの出力(パス3, パス4)
$
$FUNCTION GENERATE_DATASECINIB_TABLE$
	$IF !OMIT_IDATA$
        $tnum_datasec = 0$
		$IF CFG_PASS4$
			$tnum_datasec = PEEK(SYMBOL("tnum_datasec"), sizeof_uint32)$
		$ELSE$
			$FOREACH moid DATASEC_LIST$
				$IF MO.BASEADDR[moid] != MO.ENDADDR[moid]$
	                $tnum_datasec = tnum_datasec + 1$
				$END$
			$END$
		$END$
		$IF ISFUNCTION("DEFINE_CONST_VAR")$
			$DEFINE_CONST_VAR("const DATASECINIB", FORMAT("datasecinib_table[%d]", tnum_datasec))$ = {$NL$
		$ELSE$
			const DATASECINIB datasecinib_table[$tnum_datasec$] = {$NL$
		$END$
		$tnum_datasec = 0$
		$FOREACH moid DATASEC_LIST$
			$IF MO.BASEADDR[moid] != MO.ENDADDR[moid]$
				$tnum_datasec = tnum_datasec + 1$
				$TAB${ $MO.START[moid]$, $MO.END[moid]$,$SPC$
				&__start_$MO.ILABEL[moid]$ },$NL$
			$END$
		$END$
		};$NL$
	$ELSE$
		TOPPERS_EMPTY_LABEL(const DATASECINIB, datasecinib_table);$NL$
		$NL$
	$END$

	$IF ISFUNCTION("DEFINE_CONST_VAR")$
        $DEFINE_CONST_VAR("const uint32", "tnum_datasec")$ = $tnum_datasec$U;$NL$
	$ELSE$
		const uint32 tnum_datasec = $tnum_datasec$U;$NL$
	$END$
	$NL$
$END$

$
$  BSSセクション初期化テーブルの出力(パス3, パス4)
$
$FUNCTION GENERATE_BSSSECINIB_TABLE$
    $tnum_bsssec = 0$
	$IF CFG_PASS4$
		$tnum_bsssec = PEEK(SYMBOL("tnum_bsssec"), sizeof_uint32)$
	$ELSE$
		$FOREACH moid BSSSEC_LIST$
			$IF MO.BASEADDR[moid] != MO.ENDADDR[moid]$
	            $tnum_bsssec = tnum_bsssec + 1$
			$END$
		$END$
	$END$
	const BSSSECINIB bsssecinib_table[$tnum_bsssec$] = {$NL$
	$tnum_bsssec = 0$
	$FOREACH moid BSSSEC_LIST$
		$IF MO.BASEADDR[moid] != MO.ENDADDR[moid]$
			$tnum_bsssec = tnum_bsssec + 1$
			$TAB${ $MO.START[moid]$, $MO.END[moid]$ },$NL$
		$END$
	$END$
	};$NL$
	$NL$

	const uint32 tnum_bsssec = $tnum_bsssec$U;$NL$
	$NL$
$END$


$
$  OSアプリケーション初期化ブロックの出力
$
$FUNCTION GENERATE_OSAPINIB_TABLE$
	$IF LENGTH(OSAP.ID_LIST)$
		const OSAPINIB osapinib_table[TNUM_OSAP] = {$NL$
		$JOINEACH osapid OSAP.ID_LIST ",\n"$
			$TAB${$NL$
			$IF OSAP.TRUSTED[osapid]$
				$TAB$$TAB$TA_TRUSTED,$NL$
			$ELSE$
				$TAB$$TAB$TA_NONTRUSTED,$NL$
			$END$
			$IF OSAP.STARTUPHOOK[osapid]$
				$TAB$$TAB$&StartupHook_$osapid$,$NL$
			$ELSE$
				$TAB$$TAB$NULL,$NL$
			$END$
			$IF OSAP.ERRORHOOK[osapid]$
				$TAB$$TAB$&ErrorHook_$osapid$,$NL$
			$ELSE$
				$TAB$$TAB$NULL,$NL$
			$END$
			$IF OSAP.SHUTDOWNHOOK[osapid]$
				$TAB$$TAB$&ShutdownHook_$osapid$,$NL$
			$ELSE$
				$TAB$$TAB$NULL,$NL$
			$END$

			$IF !OMIT_OSAPMPUINFOB$
				$TAB$$TAB$$FORMAT("0x%08xU", +OSAP.BTMP[osapid])$,$NL$
				$GENERATE_OSAPINIB_MPUINFOB()$
			$ELSE$
				$TAB$$TAB$$FORMAT("0x%08xU", +OSAP.BTMP[osapid])$$NL$
			$END$

			$TAB$}
		$END$
		$NL$
		};$NL$
		$NL$
	$ELSE$
		TOPPERS_EMPTY_LABEL(const OSAPINIB, osapinib_table);$NL$
	$END$
$END$

$
$  メモリオブジェクトのベースアドレス順のリストから
$  非信頼OSAPからアクセスできない領域を除いたリスト
$  をMO_MEMTOP_ORDER_MEMINIBに入れる
$
$FUNCTION SET_MEMTOP_ORDER$
	$prev = 0$
	$FOREACH moid SORT(MO_SECTION_LIST, "MO.BASEADDR")$
	
		$IF (MO.ACPTN_R[moid] != TACP_KERNEL || MO.ACPTN_W[moid] != TACP_KERNEL || MO.ACPTN_X[moid] != TACP_KERNEL)$
			$IF !prev || (MO.LIMITADDR[prev] <= MO.BASEADDR[moid] && MO.LIMITADDR[prev] != 0)$
				$MO_MEMTOP_ORDER_MEMINIB = APPEND(MO_MEMTOP_ORDER_MEMINIB, moid)$
				$prev = moid$
			$ELSE$
$				// 同じOSAPに所属するユーザスタックの領域合併
				$IF MO.TYPE[moid] == TOPPERS_USTACK &&
					MO.TYPE[prev] == TOPPERS_USTACK &&
					MO.OSAPID[prev] == MO.OSAPID[moid]$
$					// ユーザスタック領域の併合処理
					$MO.TSKID[prev] = 0$
					$MO.TSKID[moid] = 0$
					$MO.BASE[moid] = MO.BASE[prev]$
					$MO.BASEADDR[moid] = MO.BASEADDR[prev]$
					$IF MO.LIMITADDR[prev] < MO.LIMITADDR[moid]
													&& MO.LIMITADDR[prev] != 0$
						$MO.SIZE[prev] = MO.LIMITADDR[moid] - MO.BASEADDR[prev]$
						$MO.LIMITADDR[prev] = MO.LIMITADDR[moid]$
					$ELSE$
						$MO.SIZE[moid] = MO.SIZE[prev]$
						$MO.LIMITADDR[moid] = MO.LIMITADDR[prev]$
					$END$
				$END$
			$END$
		$END$
	$END$
$END$

$
$  隣接するメモリオブジェクトのメモリ保護属性が等しい場合は統合する
$
$FUNCTION MERGE_MEMINIB$
	$merged_moid = 0$
	$FOREACH moid MO_MEMTOP_ORDER_MEMINIB$
		$IF merged_moid &&
			(CFG_PASS4 ||
			 MO.LINKER[merged_moid] && MO.LINKER[moid] &&
			 MO.MEMREG[merged_moid] == MO.MEMREG[moid]) &&
			((MO.MERGED_LIMITADDR[merged_moid] == MO.BASEADDR[moid] &&
			MO.ACPTN_R[merged_moid] == MO.ACPTN_R[moid] &&
			MO.ACPTN_W[merged_moid] == MO.ACPTN_W[moid] &&
			MO.ACPTN_X[merged_moid] == MO.ACPTN_X[moid]) ||
			MO.LIMITADDR[moid] == MO.BASEADDR[moid])$

			$MO.MERGED_LIMITADDR[merged_moid] = MO.LIMITADDR[moid]$
			$MO.MERGED_LIMIT[merged_moid] = MO.LIMIT[moid]$
		$ELSE$
			$merged_moid = moid$
			$new_list = APPEND(new_list, merged_moid)$
			$MO.MERGED_LIMITADDR[moid] = MO.LIMITADDR[moid]$
			$MO.MERGED_LIMIT[moid] = MO.LIMIT[moid]$
		$END$
	$END$
	$MO_MEMTOP_ORDER_MEMINIB = new_list$
$END$

$
$  メモリオブジェクト初期化ブロックの生成
$  第1引数 出力する配列のサイズ
$
$IF !OMIT_STANDARD_MEMINIB$
	$FUNCTION GENERATE_MEMINIB_TABLE$
		$array_size = ARGV[1]$
$		// メモリオブジェクト初期化ブロックに出力しないエントリの決定
$
$		// suppresszero：アドレス0に対応するエントリを出力しない
$		// MO.SUPPRESSLIMIT[moid]：上限アドレスのエントリを出力しない
		$prev = 0$
		$FOREACH moid MO_MEMTOP_ORDER_MEMINIB$
			$IF !prev$
				$IF MO.BASEADDR[moid] == 0 && (CFG_PASS4 || MO.LINKER[moid]) $
					$suppresszero = 1$
				$END$
			$ELSE$
				$IF MO.BASEADDR[moid] == MO.MERGED_LIMITADDR[prev] &&
					(CFG_PASS4 ||
					 MO.LINKER[prev] && MO.LINKER[moid] &&
					 MO.MEMREG[prev] == MO.MEMREG[moid])$

					$MO.SUPPRESSLIMIT[prev] = 1$
				$END$
			$END$
			$prev = moid$
		$END$
		$IF MO.MERGED_LIMITADDR[prev] == 0 && CFG_PASS4$
			$MO.SUPPRESSLIMIT[prev] = 1$
		$END$

		$tnum_meminib = 0$

$		// memtop_tableの生成
		void * const memtop_table[$array_size$] = {$NL$
		$IF !LENGTH(suppresszero)$
			$tnum_meminib = tnum_meminib + 1$
			$TAB$0,$NL$
		$END$
		$JOINEACH moid MO_MEMTOP_ORDER_MEMINIB ",\n"$
			$tnum_meminib = tnum_meminib + 1$
			$TAB$$MO.START[moid]$
			$SPC$/* $MO.ACPTN_R[moid]$, $MO.ACPTN_W[moid]$, $MO.ACPTN_X[moid]$ */
			$IF !LENGTH(MO.SUPPRESSLIMIT[moid])$
				$tnum_meminib = tnum_meminib + 1$
				,$NL$$TAB$$MO.MERGED_LIMIT[moid]$
			$END$
		$END$
		$IF CFG_PASS4 && tnum_meminib < array_size$
			$FOREACH i RANGE(tnum_meminib + 1, array_size)$
				,$NL$$TAB$NULL
			$END$
		$END$$NL$
		};$NL$
		$NL$

$		// meminib_tableの生成
		const MEMINIB meminib_table[$array_size$] = {$NL$
		$IF !LENGTH(suppresszero)$
			$TAB${ TA_NULL, TACP_KERNEL, TACP_KERNEL, TACP_KERNEL },$NL$
		$END$
		$JOINEACH moid MO_MEMTOP_ORDER_MEMINIB ",\n"$
			$TAB${
			$IF MO.TYPE[moid] == TOPPERS_USTACK$
				$SPC$TOPPERS_USTACK,
			$ELSE$
				$SPC$TA_NULL,
			$END$
			$SPC$$MO.ACPTN_R[moid]$,
			$SPC$$MO.ACPTN_W[moid]$,
			$SPC$$MO.ACPTN_X[moid]$
			$SPC$}
			$IF !LENGTH(MO.SUPPRESSLIMIT[moid])$
				,$NL$$TAB${ TA_NULL, TACP_KERNEL, TACP_KERNEL, TACP_KERNEL }
			$END$
		$END$
		$IF CFG_PASS4 && tnum_meminib < array_size$
			$FOREACH i RANGE(tnum_meminib + 1, array_size)$
				,$NL$$TAB${ 0U, 0U, 0U, 0U }
			$END$
		$END$$NL$
		};$NL$

$		// tnum_meminibの生成
		const uint32 tnum_meminib = $tnum_meminib$U;$NL$
		$NL$
	$END$
$END$

$
$  リンカで配置しないメモリオブジェクトの開始と終端のアドレスを値とした
$  変数を出力する
$
$FUNCTION OUTPUT_MO_ADDR$
	$FOREACH moid MO_SECTION_LIST$
		$IF !MO.LINKER[moid]$
			$DEFINE_VAR_SEC("._kernel_dummy_section", "void *", CONCAT("_kernel_mo_baseaddr_", moid), MO.START[moid])$
			$DEFINE_VAR_SEC("._kernel_dummy_section", "void *", CONCAT("_kernel_mo_limitaddr_", moid), MO.LIMIT[moid])$
		$END$
	$END$
$END$

$
$  OUTPUT_MO_ADDRで出力した変数の値を読み込んで
$  MO.BASEADDRとMO.LIMITADDRを設定する
$
$FUNCTION SET_MO_ADDR$
	$FOREACH moid MO_SECTION_LIST$
		$IF MO.LINKER[moid]$
			$MO.BASEADDR[moid] = SYMBOL(REGEX_REPLACE(MO.START[moid], "^&", ""))$
			$MO.LIMITADDR[moid] = SYMBOL(REGEX_REPLACE(MO.LIMIT[moid], "^&", ""))$
			$MO.ENDADDR[moid] = SYMBOL(REGEX_REPLACE(MO.END[moid], "^&", ""))$
		$ELSE$
			$MO.BASEADDR[moid]  = PEEK(SYMBOL(CONCAT("_kernel_mo_baseaddr_",  moid)), sizeof_void_ptr)$
			$MO.LIMITADDR[moid] = PEEK(SYMBOL(CONCAT("_kernel_mo_limitaddr_", moid)), sizeof_void_ptr)$
		$END$
	$END$
$END$
