/*
 *  TOPPERS ATK2
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 *      Automotive Kernel Version 2
 *
 *  Copyright (C) 2000-2003 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 *  Copyright (C) 2004-2013 by Center for Embedded Computing Systems
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2011-2013 by FUJISOFT INCORPORATED, JAPAN
 *  Copyright (C) 2011-2013 by FUJITSU VLSI LIMITED, JAPAN
 *  Copyright (C) 2011-2013 by NEC Communication Systems, Ltd., JAPAN
 *  Copyright (C) 2011-2013 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
 *  Copyright (C) 2011-2013 by Renesas Electronics Corporation, JAPAN
 *  Copyright (C) 2011-2013 by Sunny Giken Inc., JAPAN
 *  Copyright (C) 2011-2013 by TOSHIBA CORPORATION, JAPAN
 *  Copyright (C) 2004-2013 by Witz Corporation, JAPAN
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 *  $Id: sample2.c 7245 2013-03-27 06:08:24Z fsi-dankei $
 */

/*
 *		非信頼OSアプリケーション所属サンプルプログラムの本体
 *
 *  サンプルプログラムの動作はsample1.cのコメント参照
 *  本ファイルは非信頼OSアプリケーションに所属している
 *  オブジェクトを記述している
 */

#include "sample.h"
#include "sample2.h"

#define GetHwCnt(x, y)

/*
 *  サービスコールのエラーのログ出力
 */
LOCAL_INLINE void
svc_perror(const char8 *file, sint32 line, const char8 *expr, StatusType ercd)
{
	if (ercd > 0U) {
		t_perror(LOG_ERROR, file, line, expr, ercd);
	}
}

#define	SVC_PERROR(expr)	svc_perror(__FILE__, __LINE__, # expr, (expr))

/*
 *  内部関数プロトタイプ宣言
 */
uint8 GetCommand(void);
void PutActTsk(uint8 task_no);
void PutActNonPriTsk(void);
void PutTermTsk(uint8 task_no);
void PutChainTsk(uint8 from_task_no, uint8 to_task_no);
void PutSchedule(void);
void PutTaskID(void);
void PutTaskState(uint8 task_no);
void PutDisAllInt(void);
void PutSusAllInt(void);
void PutSusOSInt(void);
void PutHwCnt3(void);
void PutGetIntRes(void);
void PutGetTskRes(void);
void PutRelTskRes(void);
void PutSetEvt(uint8 task_no);
void PutClrEvt(uint8 task_no);
void PutGetEvt(uint8 task_no);
void PutWaitEvt(uint8 task_no);
void PutArmBase(void);
void PutArmTick(void);
void PutSetRel(uint8 alarm_no, uint8 tick_no, uint8 cycle_no);
void PutCanArm(void);
void PutAppMode(void);
void schedule_table_sample_routine(void);

/*
 *  内部データバッファ
 */
static volatile uint8		command_tbl[14];         /* コマンド引渡しテーブル */

/*
 *  内部定数データテーブル
 */
/* 無効イベントマスク値 */
#define invalid_mask (EventMaskType) (0)

/* イベントマスクテーブル */
static const EventMaskType	event_mask_tbl[5] = {
	invalid_mask,
	T2Evt,
	T3Evt,
	invalid_mask,
	invalid_mask
};

/* タスクIDテーブル */
static const TaskType		task_id_tbl[14] = {
	Task1,
	Task2,
	Task3,
	Task4,
	Task5,
	Task6,
	Task7,
	Task8,
	Task9,
	Task10,
	Task11,
	Task12,
	Task13,
	Task14
};

/* アラームIDテーブル */
static const AlarmType		alarm_id_tbl[2] = {
	ActTskArm,
	SetEvtArm
};

/* ティック値テーブル */
static const TickType		tick_tbl[2] = {
	(TickType) 500,
	(TickType) 900
};

/* サイクル値テーブル */
static const TickType		cycle_tbl[2] = {
	(TickType) 0,
	(TickType) 500
};

/* イベントマスク名文字列テーブル */
static const char8			*event_name_tbl[5] = {
	"Invalid",
	"T2Evt",
	"T3Evt",
	"Invalid",
	"Invalid"
};

/* タスク名文字列テーブル */
static const char8			*task_name_tbl[14] = {
	"Task1",
	"Task2",
	"Task3",
	"Task4",
	"Task5",
	"Task6",
	"Task7",
	"Task8",
	"Task9",
	"Task10",
	"Task11",
	"Task12"
	"Task13"
	"Task14"
};

/* タスク状態文字列テーブル */
static const char8			*task_state_tbl[4] = {
	"SUSPENDED",
	"RUNNING",
	"READY",
	"WAITING"
};

/* アラーム名文字列テーブル */
static const char8			*alarm_name_tbl[2] = {
	"ActTskArm",
	"SetEvtArm"
};

DEFINE_VAR_USTACK(StackType, stack_1[COUNT_STK_T(0x200)], ".stack_section");
DEFINE_VAR_SSTACK(StackType, stack_2[COUNT_STK_T(0x200)]);


/*
 *  メインタスク
 *
 *  ユーザコマンドの受信と，コマンドごとの処理実行
 */
TASK(MainTask)
{
	uint8					command;
	uint8					task_no;
	uint32					i;
	struct parameter_struct	local;

	TickType				val = 0U;
	TickType				eval = 0U;

	/*
	 *  タスク番号・コマンドバッファ初期化
	 */
	task_no = (uint8) (0);
	for (i = 0U; i < (sizeof(command_tbl) / sizeof(command_tbl[0])); i++) {
		command_tbl[i] = 0U;
	}

	/*
	 *  コマンド実行ループ
	 */
	while (1) {
		WaitEvent(MainEvt);     /* 100msの作業時間待ち */
		ClearEvent(MainEvt);

		/*
		 *  入力コマンド取得
		 */
		syslog(LOG_INFO, "Input Command:");
		command = GetCommand();

		/*
		 *  入力コマンドチェック
		 */
		if ((command <= (uint8) (0x1fU)) || (command >= (uint8) (0x80U))) {
			syslog(LOG_INFO, "Not ASCII character");
		}
		else {
#ifndef OMIT_ECHO
			syslog(LOG_INFO, "%c", command);
#endif /* OMIT_ECHO */

			/*
			 *  コマンド判別
			 */
			switch (command) {
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				/*
				 *  処理対象タスクの変更
				 */
				task_no = (uint8) (command - '1');
				break;
			case 'A':
			case '!':
			case '"':
			case '#':
			case '$':
			case '%':
			case 'z':
			case 'k':
			case 'K':
			case 'l':
			case 'i':
			case 'w':
			case 'W':
				/*
				 *  タスクへのコマンド通知
				 */
				command_tbl[task_no] = command;
				break;
			/*
			 *  以降はメインタスクでコマンド処理
			 */
			case 'a':
				PutActTsk(task_no);
				break;
			case 's':
				PutSchedule();
				break;
			case 'S':
				PutActNonPriTsk();
				break;
			case 'Z':
				PutTaskState(task_no);
				break;
			case 'x':
				syslog(LOG_INFO, "call RAISE_CPU_EXCEPTION");
				RAISE_CPU_EXCEPTION;
				break;
			case 'd':
				PutDisAllInt();
				break;
			case 'D':
				PutSusAllInt();
				break;
			case 'f':
				PutSusOSInt();
				break;
			case 'T':
				PutHwCnt3();
				break;
			case 'e':
				PutSetEvt(task_no);
				break;
			case 'E':
				PutGetEvt(task_no);
				break;
			case 'b':
				PutArmBase();
				break;
			case 'B':
				PutArmTick();
				PutArmTick();
				break;
			case 'v':
				/* SetRelAlarm(ActTskArm, 500, 0)を実行 */
				PutSetRel(0U, 0U, 0U);
				break;
			case 'V':
				/* SetRelAlarm(SetEvtArm, 500, 0)を実行 */
				PutSetRel(1U, 0U, 0U);
				break;
			case 'h':
				/* CancelAlarm(CallBackArm)を実行 */
				PutCanArm();
				break;
			case 'c':
				syslog(LOG_INFO, "Call IncrementCounter(SampleCnt)");
				IncrementCounter(SampleCnt);
				break;
			case 'C':
				syslog(LOG_INFO, "Call IncrementCounter(SampleCnt2)");
				IncrementCounter(SampleCnt2);
				break;
			case 'j':
				syslog(LOG_INFO, "GetCounterValue(SysTimerCnt, val)");
				GetCounterValue(SysTimerCnt, &val);
				syslog(LOG_INFO, " val = %d", val);
				break;
			case 'J':
				syslog(LOG_INFO, "Pre val = %d", val);
				syslog(LOG_INFO, "GetElapsedValue(SysTimerCnt, val ,eval)");
				GetElapsedValue(SysTimerCnt, &val, &eval);
				syslog(LOG_INFO, " val = %d", val);
				syslog(LOG_INFO, " eval = %d", eval);
				break;
			case 'r':
				syslog(LOG_INFO, "GetISRID() Call from Task Context");
				syslog(LOG_INFO, "GetISRID() = %d", GetISRID());
				break;
			case 'p':
				PutAppMode();
				break;
			case 't':
				schedule_table_sample_routine();
				break;
			case 'q':
				local.ercd = E_OK;
				CallTrustedFunction(tfnt3, &local); /* ShutdownOS( E_OK ) */
				break;
			case 'Q':
				local.ercd = E_OS_STATE;
				CallTrustedFunction(tfnt3, &local); /* ShutdownOS( E_OS_STATE ) */
				break;
			case 'y':
				PutActTsk(10U);
				break;
			case 'Y':
				PutActTsk(11U);
				break;
			default:
				/* 上記のコマンド以外の場合，処理を行わない */
				break;
			}
		}
	}

	/*
	 *  ここにはこない
	 */
	syslog(LOG_INFO, "MainTask TERMINATE");
	error_log(TerminateTask());
}   /* TASK( MainTask ) */

/*
 *  最高優先度タスク
 *
 *  各タスクのプリエンプト確認用
 */
TASK(HighPriorityTask)
{
	syslog(LOG_INFO, "HighPriorityTask ACTIVATE");
	error_log(TerminateTask());
}   /* TASK( HighPriorityTask ) */

/*
 *  並列実行タスク1
 */
TASK(Task1)
{
	TaskProk(0U);
}   /* TASK( Task1 ) */


/*
 *  並列実行タスク2
 */
TASK(Task2)
{
	TaskProk(1U);
}   /* TASK( Task2 ) */


/*
 *  並列実行タスク3
 */
TASK(Task3)
{
	TaskProk(2U);
}   /* TASK( Task3 ) */


/*
 *  並列実行タスク4
 */
TASK(Task4)
{
	TaskProk(3U);
}   /* TASK( Task4 ) */


/*
 *  並列実行タスク5
 */
TASK(Task5)
{
	TaskProk(4U);
}   /* TASK( Task5 ) */


/*
 *  並列実行タスク内部処理
 *
 *  メインタスクから通知されたコマンドごとの処理実行
 */
void
TaskProk(uint8 task_no)
{
	uint8 command;          /* コマンド退避バッファ */

	/*
	 *  タスク起動ログ出力
	 */
	syslog(LOG_INFO, "%s ACTIVATE", task_name_tbl[task_no]);

	/*
	 *  コマンド実行ループ
	 */
	while (1) {

		/*
		 *  コマンド取得
		 */
		while (command_tbl[task_no] == '\0') {
		}
		command = command_tbl[task_no];
		command_tbl[task_no] = 0U;

		/*
		 *  コマンド判定
		 */
		switch (command) {
		case 'A':
			PutTermTsk(task_no);
			break;
		case '!':
		case '"':
		case '#':
		case '$':
		case '%':
			PutChainTsk(task_no, (command - '!'));
			break;
		case 'z':
			PutTaskID();
			break;
		case 'k':
			PutGetTskRes();
			break;
		case 'K':
			PutRelTskRes();
			break;
		case 'i':
			PutGetIntRes();
			break;
		case 'w':
			if (task_no < 5U) {
				PutClrEvt(task_no);
			}
			break;
		case 'W':
			if (task_no < 5U) {
				PutWaitEvt(task_no);
			}
			break;
		default:
			/* 上記のコマンド以外の場合，処理は行わない */
			break;
		}
	}
}   /* TaskProk */

/*
 *  コマンド受信処理
 */
uint8
GetCommand(void)
{
	uint8 command;          /* コマンド受信バッファ */

	/*
	 *  コマンドを受信するまでループ
	 */
	command = '\0';
	do {
		WaitEvent(MainEvt);     /* 100msウェイト */
		ClearEvent(MainEvt);
		RecvPolSerialChar(&command);    /* 受信バッファポーリング */
		if (command == '\n') {
			command = '\0';
		}
	} while (command == '\0');


	return(command);
}   /* GetCommand */

/*
 *  ActivateTask 実行・ログ出力処理
 */
void
PutActTsk(uint8 task_no)
{
	struct parameter_struct local;

	syslog(LOG_INFO, "Call ActivateTask(%s)", task_name_tbl[task_no]);

	if (task_no < 5U) {
		error_log(ActivateTask(task_id_tbl[task_no]));
	}
	else {
		local.task_no = task_id_tbl[task_no];
		CallTrustedFunction(tfnt4, &local); /* ActivateTask(task_name_tbl[task_no]) */
	}

}   /* PutActTsk	*/

/*
 *  ActivateTask 実行(NonPriTask)・ログ出力処理
 */
void
PutActNonPriTsk(void)
{
	struct parameter_struct local;

	syslog(LOG_INFO, "Call ActivateTask(NonPriTask)");

	local.task_no = NonPriTask;
	CallTrustedFunction(tfnt4, &local); /* ActivateTask(NonPriTask) */

}   /* PutActNonPriTsk	*/

/*
 *  TerminateTask 実行・ログ出力処理
 */
void
PutTermTsk(uint8 task_no)
{
	StatusType ercd;        /* エラーコード */

	syslog(LOG_INFO, "%s TERMINATE", task_name_tbl[task_no]);

	ercd = TerminateTask();
	ShutdownOS(ercd);
}

/*
 *  ChainTask 実行・ログ出力処理
 */
void
PutChainTsk(uint8 from_task_no, uint8 to_task_no)
{
	StatusType ercd;            /* エラーコード */

	syslog(LOG_INFO, "Call ChainTask(%s)", task_name_tbl[to_task_no]);
	syslog(LOG_INFO, "%s TERMINATE", task_name_tbl[from_task_no]);

	ercd = ChainTask(task_id_tbl[to_task_no]);
	if (ercd == E_OS_LIMIT) {
		syslog(LOG_INFO, "Call TerminateTask()");
		syslog(LOG_INFO, "Because of ChainTask E_OS_LIMIT return");
		ercd = TerminateTask();
	}
	ShutdownOS(ercd);
}   /* PutChainTsk */

/*
 *  Schedule 実行・ログ出力処理
 */
void
PutSchedule(void)
{
	syslog(LOG_INFO, "Call ActivateTask(HighPriorityTask)");

	error_log(ActivateTask(HighPriorityTask));
	syslog(LOG_INFO, "Call Schedule()");

	error_log(Schedule());
	syslog(LOG_INFO, "Retrun Schedule()");
}   /* PutSchedule	*/

/*
 *  GetTaskID 実行・ログ出力処理
 */
void
PutTaskID(void)
{
	TaskType task_id;           /* タスクID取得バッファ */

	error_log(GetTaskID(&task_id));

	syslog(LOG_INFO, "TaskID:%d", task_id);
}   /* PutTaskID	*/

/*
 *  GetTaskState 実行・ログ出力処理
 *  Task1〜Task5，Task7取得時エラーとならない
 *  Task6取得時エラーとなる
 */
void
PutTaskState(uint8 task_no)
{
	TaskStateType state;        /* タスクID取得バッファ */

	error_log(GetTaskState(task_id_tbl[task_no], &state));

	syslog(LOG_INFO, task_name_tbl[task_no]);
	syslog(LOG_INFO, " State:%s", task_state_tbl[state]);
}   /* PutTaskState	*/

/*
 *  DisableAllInterrupts/EnableAllInterrupts 実行・ログ出力処理
 */
void
PutDisAllInt(void)
{
	syslog(LOG_INFO, "Call DisableAllInterrupts");

	DisableAllInterrupts();

	PutHwCnt3();
	syslog(LOG_INFO, "Call EnableAllInterrupts");

	EnableAllInterrupts();
}   /* PutDisAllInt	*/

/*
 *  SuspendAllInterrupts/ResumeAllInterrupts 実行・ログ出力処理
 */
void
PutSusAllInt(void)
{
	syslog(LOG_INFO, "Call SuspendAllInterrupts");

	SuspendAllInterrupts();

	PutHwCnt3();
	syslog(LOG_INFO, "Call SuspendAllInterrupts");

	SuspendAllInterrupts();

	PutHwCnt3();
	syslog(LOG_INFO, "Call ResumeAllInterrupts");

	ResumeAllInterrupts();

	PutHwCnt3();
	syslog(LOG_INFO, "Call ResumeAllInterrupts");

	ResumeAllInterrupts();
}   /* PutSusAllInt	*/

/*
 *  SuspendOSInterrupts/ResumeOSInterrupts 実行・ログ出力処理
 */
void
PutSusOSInt(void)
{
	syslog(LOG_INFO, "Call SuspendOSInterrupts");

	SuspendOSInterrupts();

	PutHwCnt3();
	syslog(LOG_INFO, "Call SuspendOSInterrupts");

	SuspendOSInterrupts();

	PutHwCnt3();
	syslog(LOG_INFO, "Call SuspendAllInterrupts");

	SuspendAllInterrupts();

	PutHwCnt3();
	syslog(LOG_INFO, "Call ResumeAllInterrupts");

	ResumeAllInterrupts();

	PutHwCnt3();
	syslog(LOG_INFO, "Call ResumeOSInterrupts");

	ResumeOSInterrupts();

	PutHwCnt3();
	syslog(LOG_INFO, "Call ResumeOSInterrupts");

	ResumeOSInterrupts();
}   /* PutSusOSInt */

/*
 *  割込み動作テスト用HWカウンタ値のログ出力処理
 */
void
PutHwCnt3(void)
{
	uint32	isr1_cnt = 0U;      /* C1ISR カウント値取得バッファ */
	uint32	isr2_cnt = 0U;      /* C2ISR カウント値取得バッファ */
	uint8	cnt;                /* 出力回数カウンタ */

	for (cnt = 0U; cnt < 3U; cnt++) {
		GetHwCnt(&isr1_cnt, &isr2_cnt);
		syslog(LOG_INFO, "C1ISR Cnt:%d, C2ISR Cnt:%d",
			   isr1_cnt, isr2_cnt);
	}
}   /* PutHwCnt3 */

/*
 *  GetResource/ReleaseResource 実行(割込みレベル)・ログ出力処理
 */
void
PutGetIntRes(void)
{
	syslog(LOG_INFO, "Call GetResource(IntLevelRes)");
	error_log(GetResource(IntLevelRes));

	PutHwCnt3();
	syslog(LOG_INFO, "Call ReleaseResource(IntLevelRes)");

	error_log(ReleaseResource(IntLevelRes));
}   /* PutGetIntRes	*/

/*
 *  GetResource 実行(タスクレベル)・ログ出力処理
 */
void
PutGetTskRes(void)
{
	syslog(LOG_INFO, "Call GetResource(TskLevelRes)");

	error_log(GetResource(TskLevelRes));
}   /* PutGetTskRes */

/*
 *  ReleaseResource 実行(タスクレベル)・ログ出力処理
 */
void
PutRelTskRes(void)
{
	syslog(LOG_INFO, "Call ReleaseResource(TskLevelRes)");

	error_log(ReleaseResource(TskLevelRes));
}

/*
 *  SetEvent 実行・ログ出力処理
 */
void
PutSetEvt(uint8 task_no)
{
	syslog(LOG_INFO, "Call SetEvent(%s, %s)",
		   task_name_tbl[task_no], event_name_tbl[task_no]);

	error_log(SetEvent(task_id_tbl[task_no], event_mask_tbl[task_no]));
}   /* PutSetEvt */

/*
 *  ClearEvent 実行・ログ出力処理
 */
void
PutClrEvt(uint8 task_no)
{
	syslog(LOG_INFO, "Call ClearEvent(%s)", event_name_tbl[task_no]);

	error_log(ClearEvent(event_mask_tbl[task_no]));
}   /* PutClrEvt */

/*
 *  GetEvent 実行・ログ出力処理
 */
void
PutGetEvt(uint8 task_no)
{
	EventMaskType mask;             /* イベントマスク取得バッファ */

	error_log(GetEvent(task_id_tbl[task_no], &mask));

	syslog(LOG_INFO, "%s Event Mask:0x%x", task_name_tbl[task_no], mask);
}   /* PutGetEvt */

/*
 *  WaitEvent 実行・ログ出力処理
 */
void
PutWaitEvt(uint8 task_no)
{
	syslog(LOG_INFO, "Call WaitEvent(%s)", event_name_tbl[task_no]);

	error_log(WaitEvent(event_mask_tbl[task_no]));
	syslog(LOG_INFO, "Return WaitEvent(%s)", event_name_tbl[task_no]);
}   /* PutWaitEvt */

/*
 *  GetAlarmBase 実行・ログ出力処理
 */
void
PutArmBase(void)
{
	AlarmBaseType info;             /* アラームベース情報取得バッファ */

	error_log(GetAlarmBase(MainCycArm, &info));

	syslog(LOG_INFO, "MainCycArm Base:");
	syslog(LOG_INFO, "\tMAXALLOWEDVALUE=%d", info.maxallowedvalue);
	syslog(LOG_INFO, "\tTICKSPERBASE=%d", info.ticksperbase);
	syslog(LOG_INFO, "\tMINCYCLE=%d", info.mincycle);
}   /* PutArmBase */

/*
 *  PutArmTick 実行・ログ出力処理
 */
void
PutArmTick(void)
{
	TickType tick;              /* 残りティック取得バッファ */

	error_log(GetAlarm(MainCycArm, &tick));

	syslog(LOG_INFO, "MainCycArm Tick:%d", tick);
}   /* PutArmTick */

/*
 *  SetRelAlarm 実行・ログ出力処理
 */
void
PutSetRel(uint8 alarm_no, uint8 tick_no, uint8 cycle_no)
{
	syslog(LOG_INFO, "Call SetRelAlarm(%s, %d, %d)",
		   alarm_name_tbl[alarm_no], tick_tbl[tick_no], cycle_tbl[cycle_no]);

	error_log(SetRelAlarm(alarm_id_tbl[alarm_no],
						  tick_tbl[tick_no], cycle_tbl[cycle_no]));
}   /* PutSetRel	*/

/*
 *  CancelAlarm 実行・ログ出力処理
 */
void
PutCanArm(void)
{
	syslog(LOG_INFO, "Call CancelAlarm(ActTskArm)");

	error_log(CancelAlarm(ActTskArm));
}   /* PutCanArm */

/*
 *  GetActiveApplicationMode 実行・ログ出力処理
 */
void
PutAppMode(void)
{
	switch (GetActiveApplicationMode()) {
	case AppMode1:
		syslog(LOG_INFO, "ActiveApplicationMode:AppMode1");
		break;
	case AppMode2:
		syslog(LOG_INFO, "ActiveApplicationMode:AppMode2");
		break;
	case AppMode3:
		syslog(LOG_INFO, "ActiveApplicationMode:AppMode3");
		break;
	default:
		syslog(LOG_INFO, "ActiveApplicationMode:Non");
		break;
	}
}   /* PutAppMode */

/*
 *  スケジュールテーブルテスト用メインループ
 *
 *  ユーザコマンドの受信と，コマンドごとの処理実行
 */
void
schedule_table_sample_routine(void)
{
	uint8					command;                /* コマンドバッファ */
	ScheduleTableType		scheduletable_id;       /* コマンド引数バッファ */
	ScheduleTableStatusType	status;                 /* スケジュール状態引数 */
	TickType				val;                    /* カウンタの現在値 */
	uint8					flag = FALSE;           /* リターンするか判定するためのフラグ */

	syslog(LOG_INFO, "\t[ schedule table sample routine IN, press 't' OUT ]");
	syslog(LOG_INFO, "");

	scheduletable_id = scheduletable1;
	/*
	 *  コマンド実行ループ
	 */
	while (1) {

		WaitEvent(MainEvt);     /* 100msの作業時間待ち */
		ClearEvent(MainEvt);

		/*
		 *  入力コマンド取得
		 */
		syslog(LOG_INFO, "Input Command:");
		command = GetCommand();
		syslog(LOG_INFO, "%c", command);

		/*
		 *  コマンド判定
		 */
		switch (command) {
		case '1':
			scheduletable_id = scheduletable1;
			break;
		case '2':
			scheduletable_id = scheduletable2;
			break;
		case 'i':
			IncrementCounter(SchtblSampleCnt);
			val = 0U;
			GetCounterValue(SchtblSampleCnt, &val);
			if ((val % 5U) == 0U) {
				syslog(LOG_INFO, "\tGetCounterValue(SchtblSampleCnt ) = %d", val);
			}
			break;
		case 's':
			syslog(LOG_INFO, "\tStartScheduleTableRel(scheduletable%d, 5)", scheduletable_id + 1U);
			error_log(StartScheduleTableRel(scheduletable_id, 5U));
			break;
		case 'S':
			syslog(LOG_INFO, "\tStartScheduleTableAbs(scheduletable%d, 5)", scheduletable_id + 1U);
			error_log(StartScheduleTableAbs(scheduletable_id, 5U));
			break;
		case 'f':
			syslog(LOG_INFO, "\tStopScheduleTable(scheduletable%d)", scheduletable_id + 1U);
			error_log(StopScheduleTable(scheduletable_id));
			break;
		case 'n':
			syslog(LOG_INFO, "\tNextScheduleTable(scheduletable%d, scheduletable%d)", scheduletable_id + 1U, scheduletable2 + 1U);
			error_log(NextScheduleTable(scheduletable_id, scheduletable2));
			break;
		case 'N':
			syslog(LOG_INFO, "\tNextScheduleTable(scheduletable%d, scheduletable%d)", scheduletable_id + 1U, scheduletable1 + 1U);
			error_log(NextScheduleTable(scheduletable_id, scheduletable1));
			break;
		case 'g':
			status = 0U;
			syslog(LOG_INFO, "\tGetScheduleTableStatus(scheduletable%d, status)", scheduletable_id + 1U);
			error_log(GetScheduleTableStatus(scheduletable_id, &status));
			syslog(LOG_INFO, "\tstatus = %d", status);
			break;
		case 't':
			syslog(LOG_INFO, "\t[ schedule table sample routine OUT, press 't' IN ]");
			flag = TRUE;
			break;
		default:
			/* コマンドが上記のケース以外なら処理は行わない */
			break;
		}
		/*  フラグが立っていた場合，リターンする  */
		if (flag == TRUE) {
			return;
		}
	}
}   /* schedule_table_sample_routine */

/*
 *  スケジュールテーブル確認用タスク6
 */
TASK(Task10)
{
	/*
	 *  タスク起動ログ出力
	 */
	syslog(LOG_INFO, "Task10 ACTIVATE");
	WaitEvent(T10Evt);
	syslog(LOG_INFO, "Task10 FINISH");
	TerminateTask();
}   /* TASK( Task10 ) */


/*
 *  スケジュールテーブル確認用タスク7
 */
TASK(Task11)
{
	/*
	 *  タスク起動ログ出力
	 */
	syslog(LOG_INFO, "Task11 ACTIVATE");
	WaitEvent(T11Evt);
	syslog(LOG_INFO, "Task11 FINISH");
	TerminateTask();
}   /* TASK( Task11 ) */


/*
 *  スケジュールテーブル確認用タスク8
 */
TASK(Task12)
{
	/*
	 *  タスク起動ログ出力
	 */
	syslog(LOG_INFO, "Task12 ACTIVATE");
	WaitEvent(T12Evt);
	syslog(LOG_INFO, "Task12 FINISH");
	TerminateTask();
}   /* TASK( Task12 ) */

/*
 *  信頼関数動作確認用タスク11
 */
TASK(Task13)
{
	struct parameter_struct	local;
	sint32					pp = 10;

	local.name1 = (sint32 *) &pp;
	local.name2 = 2;

	CallTrustedFunction(tfnt1, &local); /* 10*2 + 2 = 22 */
	syslog(LOG_NOTICE, "Task13 tfnt1 ret = %d", local.return_value);

	TerminateTask();
}   /* TASK( Task13 ) */

/*
 *  信頼関数動作確認用タスク12
 */
TASK(Task14)
{
	struct parameter_struct	local;
	sint32					pp = 20;

	local.name1 = (sint32 *) &pp;
	local.name2 = 4;

	CallTrustedFunction(tfnt2, &local); /* 20*4 + 4 = 84 */
	syslog(LOG_NOTICE, "Task14 tfnt2 ret = %d", local.return_value);

}   /* TASK( Task14 ) */

/*
 *  非信頼OSアプリケーション間アクセス権確認用タスク
 */
TASK(Task8)
{
	TaskStateType state;
	/*
	 *  タスク起動ログ出力
	 */
	syslog(LOG_INFO, "Task8 ACTIVATE");
	if (E_OK == GetTaskState(Task1, &state)) {
		syslog(LOG_INFO, "GetTaskState(Task1, &state), state = %s", task_state_tbl[state]);
	}
	syslog(LOG_INFO, "Task8 FINISH");
	TerminateTask();
}   /* TASK( Task8 ) */

/*
 *  非信頼OSアプリケーション間アクセス権確認用タスク
 */
TASK(Task9)
{
	TaskStateType state;
	/*
	 *  タスク起動ログ出力
	 */
	syslog(LOG_INFO, "Task9 ACTIVATE");
	if (E_OK == GetTaskState(Task2, &state)) {
		syslog(LOG_INFO, "GetTaskState(Task2, &state), state = %s", task_state_tbl[state]);
	}
	syslog(LOG_INFO, "Task9 FINISH");
	TerminateTask();
}   /* TASK( Task8 ) */

/*
 *  OSAP定義エラーフックルーチン
 */
#ifdef CFG_USE_OSAP_ERRORHOOK
void
ErrorHook_NT_osap2(StatusType Error)
{
	syslog(LOG_INFO, "ErrorHook_NT_osap2 Error:%s", atk2_strerror(Error));
}
#endif /* CFG_USE_OSAP_ERRORHOOK */

/*
 *  OSAP定義スタートアップフックルーチン
 */
#ifdef CFG_USE_OSAP_STARTUPHOOK
void
StartupHook_NT_osap2(void)
{
	syslog(LOG_INFO, "Sample StartupHook_NT_osap2!\n");
}
#endif /* CFG_USE_OSAP_STARTUPHOOK */

/*
 *  OSAP定義シャットダウンフックルーチン
 */
#ifdef CFG_USE_OSAP_SHUTDOWNHOOK
void
ShutdownHook_NT_osap2(StatusType Error)
{
	syslog(LOG_INFO, "Sample ShutdownHook_NT_osap2!\n");
}
#endif /* CFG_USE_OSAP_SHUTDOWNHOOK */
